<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kritiksaran extends Model
{
    protected $table = "kritiksaran";

    public function user(){
        return $this->belongsTo('App\Model\User');
    }
}
