<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use PDF;

use function GuzzleHttp\json_encode;

class UserSparepartController extends Controller
{
    //tampil data checkout
    public function index(){
        $spr = DB::table('spareparts')->get();
        return view('user.utama.sparepart.sparepart', ['spr'=> $spr]);
    }

    //detail data
    public function detail($id){
        $spr = DB::table('spareparts')->where('id_sparepart', $id)->first();
        return view('user\utama\sparepart\datail', ['spr'=> $spr]);

    }

    //tampil keranjang
    public function keranjang(){
         $keranjang = DB::table('keranjang')
         ->where('user_id', Session::get('id'))
         ->join('spareparts', 'spareparts.id_sparepart', '=', 'keranjang.id_sparepart')

         ->get();
        return view('user\utama\sparepart\keranjang', ['keranjang'=>$keranjang]);
    }

    //tambah keranjang
    public function keranjangTambah(Request $request){
         $keranjang = DB::table('keranjang')->insert(
            [
            
            'id_sparepart' => $request->id_sparepart,
            'user_id' => Session::get('id'),
            'harga' => $request->harga,
            'jumlah' => $request->qty,
            'subtotal' => $request->harga*$request->qty
            
            ]
        );
        return json_encode(array(
            "statusCode"=>200
        ));
    }

    //hapus keranjang
    public function delete($id)
    {
        DB::table('keranjang')->where('id_keranjang', $id)->delete();
        
        return redirect('user\utama\sparepart\keranjang')->with('status', 'Data Berhasil Dihapus');
    }

   public function deleteAll(Request $request)

    {

        $id = $request->id;

        DB::table("keranjang")->whereIn('id_keranjang',explode(",",$id))->delete();

        return response()->json(['success'=>"Products Deleted successfully."]);

    }

    //ini controllernya yg json
    public function jsonPut(Request $request)
    {
        $json = file_get_contents(base_path("cart.json"));
        $dataPrev = json_decode($json,true);
        $dataArr = [
            $request->id => [
                "id" => $request->id,
                "qty" => $request->qty,
                "harga" => $request->harga
            ]
        ];
        if(!in_array($dataArr, $dataPrev['cart'], true)){
            array_push($dataPrev['cart'],$dataArr);
        }
        file_put_contents(base_path("cart.json"),json_encode($dataPrev,JSON_PRETTY_PRINT));
        return response()->json(["message"=>"Data ditambahkan!"]);
    }

    public function jsonDel(Request $request)
    {
        $dataArr = json_encode($request);
        file_put_contents(base_path("cart.json"),$dataArr);
        return response()->json(['message'=>"Added."]);
    }

    public function jsonGet()
    {
        $dataArr = file_get_contents(base_path("cart.json"));
        return response()->json(['data_cart'=>$dataArr]);
    }

    //tampil checkout
    public function checkout(){
        // $checkout = DB::table('detail_penjualan')
        //  ->where('user_id', Session::get('id'))
        //  ->join('detail_penjualan', 'keranjang.id_keranjang', '=', 'detail_penjualan.id_keranjang')

        //  ->get();
        // return view('user.utama.sparepart.checkout', ['checkout'=>$checkout]);
		return view('user.utama.sparepart.checkout');
    }

    //bayar
    public function bayar()
    {
        return view('user.utama.sparepart.bayar');
    }

    //histori checkout
    public function hcheckout()
    {
        return view('user.utama.sparepart.hCheckout');
    }


}