<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function index(){
		$konten = DB::table('konten')->where('kategori', 'Home1')->first();
        return view('user.index', ['index'=> $konten]);
	}
 
	public function layanan(){
		return view('user.layanan');
    }
    
    public function caraKerja(){
		return view('user.caraKerja');
	}
 
	public function kontak(){
		return view('user.kontak');
	}

	//tambah kritik saran
    public function addKS(Request $request)
    {
        $timestamps = true;
        //query builder insert
        DB::table('kritiksaran')->insert(
            [
                'nama' => $request->nama,
				'isi' => $request->isi,
            ]
        );
        //Redirect dengan status 
        return redirect('user/kontak')->with('status', 'Data Berhasil Ditambahkan');
    }
}