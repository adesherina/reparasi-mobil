<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;

class UserPesananController extends Controller
{
    //tampil data
    public function index(){
        
        return view('user.utama.pesanan.pesanan');
    }
    //tambah data
    public function tambah(){
        return view('user.utama.pesanan.pesananTambah');
    }

    public function generateRandomString($length = 25) {
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
               $randomString .= $characters[rand(0, $charactersLength - 1)];
             }
               return $randomString;
            }

    //simpan data service
    public function simpan(Request $request){
        $kd_service = $request->kd_service;
        $sparepart = $request->harga_sparepart;
        $hg_service = $request->harga_service;
        $input = $request->all();
        // $input['jenis_service'] =  json_encode($input['jenis_service']);
        $input['jenis_service'] = implode(', ',$input['jenis_service']);
        Service::create($input);
        $transaksi = DB::table('transaksi_service')->insert(
            [
            'nota' => $this->generateRandomString(10),
            'kd_service' => $kd_service,
            'total_harga' => $sparepart+$hg_service,
            'nama_user' => Session::get('name'),
            'tgl_transaksi' => now(),
            'status' => "unpaid"
            ]
        );
        $kode = DB::table('transaksi_service')->where('kd_service', $kd_service)->first();
         return redirect('user/utama/pesanan/reservasi/'.$kd_service);
         
    }
    
     //delete data
    public function delete($id)
    {
        DB::table('services')->where('kd_service', $id)->delete();
        return redirect('services')->with('status', 'Data Berhasil Dihapus');
    }


    //tampil data transaksi
    public function data($kd_service){

        $transaksi = DB::table('services')
            ->join('users', 'users.id', '=', 'services.user_id')
            ->where('users.id', Session::get('id'))
            ->where('kd_service', $kd_service)
            ->first();

        return view('user.utama.pesanan.bayar', ['transaksi' => $transaksi]);
        
    }

    //update data transaksi
    public function updatedata(Request $request){
        //query builder insert
        $kd_service = $request->kd_service;
        if($request->hasFile('file')){
            $resorce = $request->file('file');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/gambar", $name);
            $transaksi = DB::table('transaksi_service')
            ->where('kd_service', $kd_service)
            ->update(
                [
                    'file' => $name,
                    'status' => "unverified"
                ]);
            echo "Gambar berhasil di upload";
        }else{
            echo "Gagal upload gambar";
        }

        return redirect('user/utama/pesanan/reservasi/'.$kd_service)->with('status', 'Data Berhasil Ditambahkan'); 
         ;
    }
    
    public function reservasi($kd_service){
        return view('user.utama.pesanan.reservasi',['kd_service' => $kd_service]);
    }

    
    
}


?>