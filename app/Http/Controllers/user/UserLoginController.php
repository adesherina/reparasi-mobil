<?php

namespace App\Http\Controllers\user;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Model\UserLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class UserLoginController extends Controller
{
    public function index(){
        return view('user.login');
    }

    public function login(Request $request){
        // dd($request->all());

        $data = UserLogin::where('username', $request->username)->first();
        // $hash = Hash::make($data->password);
        // echo $data->password ."<br>". $hash;
        $hash = Hash::check($data->password, $request->password);
       
        
        if (!$data){
            return redirect('user/login')->with('message', 'username salah');
        }else{
            if(Hash::check($request->password, $data->password)){
                Session::put('name',$data->name);
                Session::put('id',$data->id);
                session(['berhasil_login' => true]);
                return redirect('user/utama/dashboard');
            }
            return redirect('user/login')->with('message', 'password salah');
        }
        
    }

    public function logout(Request $request){
        $request->session()->flush();
        return redirect('user/login');
    }
}
