<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserRegisterController extends Controller
{

    public function index(){
        return view('user.register');
    }

    public function addProcess(Request $request)
    {
        $timestamps = true;
        //query builder insert
        DB::table('users')->insert(
            [
                'name' => $request->nama,
                'no_telp' => $request->no_telp,
                'username' => $request->username,
                'password' => Hash::make($request->password),
            ]
        );
        //Redirect dengan status 
        return redirect('user/login')->with('status', 'Data Berhasil Ditambahkan');
    }
}