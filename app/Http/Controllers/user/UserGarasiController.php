<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Database\QueryException;

class UserGarasiController extends Controller
{
    //tampil data
    public function index()
    {
        return view('user.utama.kendaraan.garasi');
    }

    //tambah data
    public function tambah(){
        return view('user.utama.kendaraan.garasi');
    }

    //simpan data
    public function simpan(Request $request){
        //query builder insert
        DB::table('kendaraan')->insert(
            [
            'user_id' => $request->user_id,
            'merk_kendaraan' => $request->merk_kendaraan,
            'model' => $request->model,
            'tahun_keluar' => $request->tahun_keluar,
            'platnomer' => $request->platnomor,
            'kilometer' => $request->kilometer,
            ]
        );
        //Redirect dengan status 
       return redirect('user/utama/garasi')->with('status', 'Data Berhasil Ditambahkan');
    }

    //detail data
    public function detail($id){
        $kendaraan = DB::table('kendaraan')->where('id_kendaraan', $id)->first();
        
        return view('user.utama.kendaraan.Detail', ['kendaraan'=> $kendaraan]);

    }

    //update
    public function updateProcess(Request $request, $id)
    {
        DB::table('kendaraan')->where('id_kendaraan', $id)->update([
            'jenis_mesin' => $request->jenis_mesin,
            'transmisi' => $request->transmisi,    
            'bahan_bakar' => $request->bahan_bakar,
            'oli_mesin' => $request->oli_mesin, 
        ]);
        
        return redirect('user/utama/garasi/detail/'.$id)->with('status', 'Data Berhasil Diedit');
    }

     //delete data
    public function delete($id)
    {
        try
        {
            DB::table('kendaraan')->where('id_kendaraan', $id)->delete();
            return redirect('user/utama/garasi')->with('statusSuccess', 'Data Berhasil Dihapus');
        }
        //catch specific exception....
        catch(QueryException $e)
        {
            return redirect('user/utama/garasi')->with('statusFailed', 'Data Gagal Dihapus');
        } 
        
        
    }

}
?>