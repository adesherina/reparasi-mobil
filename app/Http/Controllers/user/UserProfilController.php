<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;


class UserProfilController extends Controller
{
    
    //View Update Form
    public function update($id)
    {
        $user = DB::table('users')->where('id', $id)->first();
        return view('user.utama.profil.edit', ['user' => $user]);
    }


    public function updateProcess(Request $request, $id){
        if($request->hasFile('foto')){
            $resorce = $request->file('foto');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/gambar/user", $name);
            DB::table('users')->where('id', $id)->update(
                [
                    'username' => $request->username,
                    'name' => $request->name,
                    'no_telp' => $request->no_telp,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'alamat' => $request->alamat,
                    'kode_pos' => $request->kode_pos,
                    'foto' => $name
                ]);
            echo "Gambar berhasil di upload";
        }else{
            echo "Gagal upload gambar";
        }
        return redirect('user/update/'.Session::get('id'))->with('status', 'Data Berhasil Diedit');
    }

    //Update Process
    public function updatePassword(Request $request, $id)
    {
        
        $newPassword = $request->newPassword;
        if ($newPassword === $request->confirmPassword) {
            DB::table('users')->where('id', $id)->update([
                'password' => Hash::make($request->newPassword),
            ]);
            return redirect()->back()->with("editpw", "Berhasil Ganti Password");
        } else {
           return redirect()->back()->with("failedpw", "Gagal Ganti Password");
        }
    }

}
