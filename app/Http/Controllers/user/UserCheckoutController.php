<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use PDF;


class UserCheckoutController extends Controller
{
    //tampil data checkout
    public function index(){
        $trans_service = DB::table('transaksi_service')
            ->join('services', 'services.kd_service', '=', 'transaksi_service.kd_service')
            ->join('users', 'users.id', '=', 'services.user_id')
            ->where('users.id', Session::get('id'))
            ->get();

        return view('user.utama.checkout.checkout', ['trans_service' => $trans_service]);
    }

    //tampil data Invoice
    public function data($nota){
        $trans_service = DB::table('transaksi_service')
            ->join('services', 'services.kd_service', '=', 'transaksi_service.kd_service')
            ->join('users', 'users.id', '=', 'services.user_id')
            ->where('users.id', Session::get('id'))
            ->where('nota', $nota)
            ->first();
        return view('user.utama.checkout.invoice', ['ts' => $trans_service]);
    }

    //cetak pdf
    public function print($nota)
        {
            $trans_service = DB::table('transaksi_service')
            ->join('services', 'services.kd_service', '=', 'transaksi_service.kd_service')
            ->join('users', 'users.id', '=', 'services.user_id')
            ->where('users.id', Session::get('id'))
            ->where('nota', $nota)
            ->first();
            $pdf = PDF::loadview('user/utama/checkout/cetak', ['ts' => $trans_service])->setPaper('A4','potrait');
            return $pdf->stream();
        }

    //delete data
    public function delete($kd)
    {
        DB::table('transaksi_service')->where('kd_service', $kd)->delete();
        DB::table('services')->where('kd_service', $kd)->delete();
        return redirect('user/utama/checkout')->with('status', 'Data Berhasil Dihapus');
    }


}