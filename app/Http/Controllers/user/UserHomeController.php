<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;

class UserHomeController extends Controller
{
    //tampil data
    public function index(){
        $services = DB::table('services')
            ->join('users', 'users.id', '=', 'services.user_id')
            ->join('kendaraan', 'kendaraan.id_kendaraan', '=', 'services.kendaraan_id')
            ->where('users.id', Session::get('id'))
            ->get();
        
            if(session('berhasil_login')){
                return view('user.utama.dashboard',compact('services'));
            }else{
                return redirect('user/login');
            }
    }
}
?>