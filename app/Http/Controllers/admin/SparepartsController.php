<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Symfony\Contracts\Service\Attribute\Required;

class SparepartsController extends Controller
{
    //tampil data
    public function index(){
        $spareparts = DB::table('spareparts')->get();
        return view('Admin.spareparts.spareparts', ['spareparts'=> $spareparts]);
    }

    //tambah data
    public function tambah(){
        return view('Admin.spareparts.sparepartsTambah');
    }

    //simpan data
    public function simpan(Request $request){
        if($request->hasFile('gambar')){
            $resorce = $request->file('gambar');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/gambar", $name);
            $save = DB::table('spareparts')->insert(
                [
                    'merk'=>$request->merk,
                    'nama_sparepart' => $request->nama_sparepart,
                    'harga_beli' => $request->harga_beli,
                    'harga_jual' => $request->harga_jual,
                    'stok' => $request->stok,
                    'deskripsi' => $request->deskripsi,
                    'gambar' => $name
                ]);
            echo "Gambar berhasil di upload";
        }else{
            echo "Gagal upload gambar";
        }
        return redirect('spareparts')->with('status', 'Data Berhasil Ditambahkan');
    }

    //edit data
    public function edit($id){
        $spareparts = DB::table('spareparts')->where('id_sparepart', $id)->first();
        return view('Admin.spareparts.sparepartsEdit', ['spareparts'=> $spareparts]);

    }

    //update
    public function update(Request $request, $id)
    {
        DB::table('spareparts')->where('id_sparepart', $id)->update([
            'merk'=>$request->merk,
            'nama_sparepart' => $request->nama_sparepart,
            'harga_beli' => $request->harga_beli,
            'harga_jual' => $request->harga_jual,
            'stok' => $request->stok,
            'deskripsi' => $request->deskripsi,
            'gambar' => $request->gambar
        ]);
        
        return redirect('spareparts')->with('status', 'Data Berhasil Diedit');

    }

    //detail data
    public function detail($id){
        $spr = DB::table('spareparts')->where('id_sparepart', $id)->first();
        return view('Admin.spareparts.Detail', ['spr'=> $spr]);

    }

    //hapus data
    public function delete($id)
    {
        DB::table('spareparts')->where('id_sparepart', $id)->delete();
        
        return redirect('spareparts')->with('status', 'Data Berhasil Dihapus');
    }

}