<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class JasaController extends Controller
{
    //tampil data
    public function index(){
        $jasa = DB::table('jasa')->get();
        return view('Admin.jasa.jasa', ['jasa'=> $jasa]);
    }

    //tambah data
    public function tambah(){
        return view('Admin.jasa.tambahJasa');
    }

    //simpan data
    public function simpan(Request $request){
        //query builder insert
        DB::table('jasa')->insert(
            [
            'nama_jasa' => $request->nama_jasa,
            'harga_jasa' => $request->harga_jasa,
            ]
        );
        //Redirect dengan status 
        return redirect('jasa')->with('status', 'Data Berhasil Ditambahkan');
    }

    //edit data
    public function edit($id){
        $jasa = DB::table('jasa')->where('id_sparepart', $id)->first();
        return view('Admin.jasa.editJasa', ['jasa'=> $jasa]);

    }

    //update
    public function update(Request $request, $id)
    {
        DB::table('jasa')->where('id_jasa', $id)->update([
            'nama_jasa' => $request->nama_jasa,
            'harga_jasa' => $request->harga_jasa,
        ]);
        
        return redirect('jasa')->with('status', 'Data Berhasil Diedit');

    }

    //hapus data
    public function delete($id)
    {
        DB::table('jasa')->where('id_jasa', $id)->delete();
        
        return redirect('jasa')->with('status', 'Data Berhasil Dihapus');
    }

}