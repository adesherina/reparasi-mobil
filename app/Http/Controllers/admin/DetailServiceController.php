<?php

namespace App\Http\Controllers\admin;

use App\model;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class DetailServiceController extends Controller
{
    public function data()
    {
        // $detail = DB::table('detail_service')
        //     ->join('users', 'users.id', '=', 'detail_service.user_id')
        //     ->join('spareparts', 'spareparts.id_sparepart', '=', 'detail_service.id_sparepart')
        //     ->get();
        $detail = DB::table('detail_service')->get();

        return view('Admin.detail.detailService', ['detail' => $detail]);
    }
}