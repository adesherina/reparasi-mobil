<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Kritiksaran;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class KritikAdminController extends Controller
{
    public function data()
    {
        $kritik = Kritiksaran::all();

        return view('Admin.KS.kritik', ['kritiksaran' => $kritik]);
    }
    
    public function delete($id)
    {
        DB::table('kritiksaran')->where('id', $id)->delete();
        return redirect('kritik')->with('status', 'Data Berhasil Dihapus');
    }

}