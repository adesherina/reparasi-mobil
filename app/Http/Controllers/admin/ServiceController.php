<?php

namespace App\Http\Controllers\admin;

use App\model;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function data()
    {
        $services = DB::table('services')
            ->join('users', 'users.id', '=', 'services.user_id')
            ->join('kendaraan', 'kendaraan.id_kendaraan', '=', 'services.kendaraan_id')
            ->get();

        return view('Admin.services.service', ['services' => $services]);
    }

    //edit data
    public function update($id){
        $services = DB::table('services')->where('kd_service', $id)->join('users', 'users.id', '=', 'services.user_id')
            ->join('kendaraan', 'kendaraan.id_kendaraan', '=', 'services.kendaraan_id')->first();
        return view('Admin.services.serviceEdit', ['services'=> $services]);

    }

    //update
    public function updateProcess(Request $request, $id)
    {
        DB::table('services')->where('kd_service', $id)->update([
            'tindakan' => $request->tindakan,     
        ]);
        
        return redirect('services')->with('status', 'Data Berhasil Diedit');

    }

    //detail data
    public function detail($id){
        $services = DB::table('services')->where('kd_service', $id)->join('users', 'users.id', '=', 'services.user_id')
            ->join('kendaraan', 'kendaraan.id_kendaraan', '=', 'services.kendaraan_id')->first();
        return view('Admin.services.serviceDetail', ['services'=> $services]);

    }

     //delete data
    public function delete($id)
    {
        DB::table('services')->where('kd_service', $id)->delete();
        return redirect('services')->with('status', 'Data Berhasil Dihapus');
    }

    
    
}