<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class JadwalController extends Controller
{
    //tampil data
    public function index(){
        $jadwal = DB::table('jadwal')
        ->join('services', 'services.nota', '=', 'jadwal.nota')
        ->join('users', 'users.id', '=', 'services.user_id')

        ->get();
        return view('Admin.jadwal.jadwal', ['jadwal'=> $jadwal]);
    }

    //tambah data
    public function tambah(){
        return view('Admin.jadwal.tambah');
    }

    //simpan data
    public function simpan(Request $request){
        //query builder insert
        DB::table('jadwal')->insert(
            [
            'jam_mulai' => $request->jam_mulai,
            'jam_selesai' => $request->jam_selesai,
            ]
        );
        //Redirect dengan status 
        return redirect('jadwal')->with('status', 'Data Berhasil Ditambahkan');
    }

    //edit data
    public function edit($id){
        $jadwal = DB::table('jadwal')->where('id_jadwal', $id)->first();
        return view('Admin.jadwal.Edit', ['jadwal'=> $jadwal]);

    }

    //update
    public function update(Request $request, $id)
    {
        DB::table('jadwal')->where('id_jadwal', $id)->update([
            'jam_mulai' => $request->jam_mulai,
            'jam_selesai' => $request->jam_selesai,
        ]);
        
        return redirect('jadwal')->with('status', 'Data Berhasil Diedit');

    }

    //hapus data
    public function delete($id)
    {
        DB::table('jadwal')->where('id_jadwal', $id)->delete();
        
        return redirect('jadwal')->with('status', 'Data Berhasil Dihapus');
    }

}