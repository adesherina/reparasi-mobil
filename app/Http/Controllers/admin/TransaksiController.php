<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class TransaksiController extends Controller
{
    //tampil data
    public function index(){
         $ts = DB::table('transaksi_service')
            ->join('services', 'services.kd_service', '=', 'transaksi_service.kd_service')
            ->join('users', 'users.id', '=', 'services.user_id')
            ->get();
        return view('Admin.transaksi.transaksi', ['ts'=> $ts]);
    }

    //edit data
    public function update($id){
        $ts = DB::table('transaksi_service')->where('nota', $id)
        ->join('services', 'services.kd_service', '=', 'transaksi_service.kd_service')
        ->join('users', 'users.id', '=', 'services.user_id')
        ->first();
        return view('Admin.transaksi.transaksiEdit', ['ts'=> $ts]);

    }

    //update
    public function updateProcess(Request $request, $id)
    {
        DB::table('transaksi_service')->where('nota', $id)->update([
            'status' => $request->status,     
        ]);
        
        return redirect('transaksi')->with('status', 'Data Berhasil Diedit');

    }

    //delete data
    public function delete($id)
    {
        DB::table('transaksi_service')->where('nota', $id)->delete();
        return redirect('transaksi')->with('status', 'Data Berhasil Dihapus');
    }


   
}