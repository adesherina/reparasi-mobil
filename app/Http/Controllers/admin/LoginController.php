<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Login;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;


class LoginController extends Controller
{
    public function index(){
        return view('Admin.login');
    }

    public function login(Request $request){
        // dd($request->all());

        $data = Login::where('username', $request->username)->first();
        
        if (!$data){
            return redirect('/login')->with('message', 'username salah');
        }else{
            if(Hash::check($request->password, $data->password)){
                Session::put('nama',$data->nama);
                Session::put('id',$data->id);

                session(['berhasil_login' => true]);
                return redirect('/dashboard');
            }
            return redirect('/login')->with('message', 'password salah');
        }
        
    }

    public function logout(Request $request){
        $request->session()->flush();
        return redirect('/login');
    }
}
