<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class KontenController extends Controller
{
    //tampil data
    public function index(){
        $konten = DB::table('konten')->paginate(5);
        return view('Admin.konten.konten', ['konten'=> $konten]);
    }

    //tambah data
    public function tambah(){
        return view('Admin.konten.kontenTambah');
    }

    //simpan data
    public function simpan(Request $request){
        //query builder insert
        DB::table('konten')->insert(
            [
            'judul_konten' => $request->judul_konten,
            'isi_konten' => $request->isi_konten,
            'kategori' => $request->kategori,
            ]
        );
        //Redirect dengan status 
        return redirect('konten')->with('status', 'Data Berhasil Ditambahkan');
    }

    //edit data
    public function edit($id){
        $konten = DB::table('konten')->where('id', $id)->first();
        return view('Admin.konten.kontenEdit', ['konten'=> $konten]);

    }

    //update
    public function update(Request $request, $id)
    {
        DB::table('konten')->where('id', $id)->update([
            'judul_konten' => $request->judul_konten,
            'isi_konten' => $request->isi_konten,
            'kategori' => $request->kategori,
        ]);
        
        return redirect('konten')->with('status', 'Data Berhasil Diedit');

    }

    //detail data
    public function detail($id){
        $konten = DB::table('konten')->where('id', $id)->first();
        return view('Admin.konten.kontenDetail', ['konten'=> $konten]);

    }

    //hapus data
    public function delete($id)
    {
        DB::table('konten')->where('id', $id)->delete();
        
        return redirect('konten')->with('status', 'Data Berhasil Dihapus');
    }

}