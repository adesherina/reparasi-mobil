$(document).ready(function () {
    $(".keranjang-detail").on("click", function () {
        var id_sparepart = $("#id_sparepart-detail").val();
        var qty = $("#qty-detail").val();
        var harga = $("#harga-detail").val();
        console.log(id_sparepart);
        if (id_sparepart != "") {
            //   $("#butsave").attr("disabled", "disabled");
            $.ajax({
                url: "/user/utama/sparepart/keranjang",
                type: "POST",
                data: {
                    _token: $("#csrf-detail").val(),
                    id_sparepart: id_sparepart,
                    qty: qty,
                    harga: harga,
                },
                cache: false,
                success: function (dataResult) {
                    console.log(dataResult);
                    var dataResult = JSON.parse(dataResult);
                    if (dataResult.statusCode == 200) {
                        swal({
                            title: "Produk telah di tambahkan ke keranjang",
                            icon: "success",
                            type: "success",
                        });
                    } else if (dataResult.statusCode == 201) {
                        alert("Error occured !");
                    }
                },
            });
        } else {
            alert("Please fill all the field !");
        }
    });

    $(".keranjang2").on("click", function () {
        var id_sparepart = $(this).data("produkid");
        var harga = $(this).data("produkharga");
        var qty = $(this).data("produkqty");
        var csrf = $(this).data("produkcsrf");

        if (id_sparepart != "") {
            //   $("#butsave").attr("disabled", "disabled");
            $.ajax({
                url: "/user/utama/sparepart/keranjang",
                type: "POST",
                data: {
                    _token: csrf,
                    id_sparepart: id_sparepart,
                    qty: qty,
                    harga: harga,
                },
                cache: false,
                success: function (dataResult) {
                    console.log(dataResult);
                    var dataResult = JSON.parse(dataResult);
                    if (dataResult.statusCode == 200) {
                        swal({
                            title: "Produk telah di tambahkan ke keranjang",
                            icon: "success",
                            type: "success",
                        });
                    } else if (dataResult.statusCode == 201) {
                        alert("Error occured !");
                    }
                },
            });
        } else {
            alert("Please fill all the field !");
        }
    });
});
