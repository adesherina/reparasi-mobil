//tab
function openService(evt, serviceName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(serviceName).style.display = "block";
    evt.currentTarget.className += " active";
}

//menampilkan harga service
function myFunction() {
    var wbst = [];
    var jasa = [];
    var total_harga = 0;
    $("#my-select option:selected").each(function () {
        total_harga += parseFloat($(this).attr("id"));
        jasa.push($(this).attr("id"));
        wbst.push($(this).val());
        // coba = wbst.toString($(this).val());
        // if (wbst.pop($(this).val())) {
        //     document.getElementById("coba").textContent = "";
        // } else if (wbst.push($(this).val())) {
        //     document.getElementById("coba").textContent = "gagal";
        // }
    });
    var x = wbst.toString();
    document.getElementById("total").value = total_harga;

    var n = x.search("Oli");
    var aki = x.search("Aki");
    var tone = x.search("Tone");

    if (aki == -1 && n == -1) {
        document.getElementById("harga_sparepart").value = 0;
        document.getElementById("notif").innerHTML = `
             <p> Keranjang Kosong </p>
        `;
    }
    if (tone == -1 && aki != -1 && n != -1) {
        document.getElementById("harga_sparepart").value = 90000;
        document.getElementById("notif").innerHTML = `
            <p> Harga Jasa Ganti Aki : ${jasa[0]}</p>
            <p> Harga Aki : Rp. 45.000,00</p>
            <p> Harga Jasa Ganti Oli : ${jasa[1]}</p>
            <p> Harga oli : Rp.45.000,00</p>
        `;
    } else if (tone == -1 && aki == 6 && n == -1) {
        document.getElementById("harga_sparepart").value = 45000;
        document.getElementById("notif").innerHTML = `
            <p> Harga Jasa Ganti Aki : ${jasa[0]}</p>
            <p> Harga Aki : Rp. 45.000,00</p>
        `;
    } else if (tone == -1 && aki == -1 && n != -1) {
        document.getElementById("harga_sparepart").value = 45000;
        document.getElementById("notif").innerHTML = `
            <p> Harga Jasa Ganti Oli : ${jasa[0]}</p>
            <p> Harga oli : Rp.45.000,00</p>
        `;
    } else if (tone == 0 && aki == -1 && n == -1) {
        document.getElementById("harga_sparepart").value = 0;
        document.getElementById("notif").innerHTML = `
            <p> Harga Jasa Tone Up : ${jasa[0]}</p>
        `;
    } else if (tone == 0 && aki != -1 && n == -1) {
        document.getElementById("harga_sparepart").value = 45000;
        document.getElementById("notif").innerHTML = `
            <p> Harga Jasa Tone Up : ${jasa[0]}</p>
            <p> Harga Jasa Ganti Aki : ${jasa[1]}</p>
            <p> Harga Aki : Rp. 45.000,00</p>
        `;
    } else if (tone == 0 && aki == -1 && n != -1) {
        document.getElementById("harga_sparepart").value = 45000;
        document.getElementById("notif").innerHTML = `
            <p> Harga Jasa Tone Up : ${jasa[0]}</p>
            <p> Harga Jasa Ganti Oli : ${jasa[1]}</p>
            <p> Harga oli : Rp.45.000,00</p>
        `;
    } else if (tone == 0 && aki != -1 && n != -1) {
        document.getElementById("harga_sparepart").value = 90000;
        document.getElementById("notif").innerHTML = `
            <p> Harga Jasa Tone Up : ${jasa[0]}</p>
            <p> Harga Jasa Ganti Aki : ${jasa[1]}</p>
            <p> Harga Aki : Rp. 45.000,00</p>
            <p> Harga Jasa Ganti Oli : ${jasa[2]}</p>
            <p> Harga oli : Rp.45.000,00</p>
        `;
    } else if (tone == 0 && aki == -1 && n != -1) {
        document.getElementById("harga_sparepart").value = 45000;
        document.getElementById("notif").innerHTML = `
        <p> Harga Jasa Tone Up : ${jasa[0]}</p>
            <p> Harga Jasa Ganti Aki : ${jasa[1]}</p>
            <p> Harga Aki : Rp. 45.000,00</p>
        `;
    }

    // document.getElementById("coba").value = tone + " " + aki + " " + n;
}

//menghitung total harga
// function hitung() {
//     harga_jual = parseInt($("#harga_jual").val());
//     qty = parseInt($("#qty").val());
//     subtotal = harga_jual + qty;
//     $(".pesan").empty().append("subtotal:");
//     $(".pesan").append(subtotal);
// }

// $("#harga_jual, #qty").keyup(function () {
//     hitung();
// });

//alret checkout
$("#deleteCheckout").on("click", function (event) {
    event.preventDefault();
    const url = $(this).attr("href");
    swal({
        title: "Apakah kamu yakin?",
        text: "Data Service kamu akan dihapus permanen",
        icon: "warning",
        buttons: ["Batal", "Ya!"],
    }).then(function (value) {
        if (value) {
            window.location.href = url;
        }
    });
});

//alert keranjang
$(".deleteKeranjang").each(function (index, value) {
    $(this).on("click", function (event) {
        event.preventDefault();
        const url = $(this).attr("href");
        swal({
            title: "Apakah kamu yakin?",
            text: "Produk ini akan di hapus?",
            icon: "warning",
            buttons: ["Batal", "Ya!"],
        }).then(function (value) {
            if (value) {
                window.location.href = url;
            }
        });
    });
});
