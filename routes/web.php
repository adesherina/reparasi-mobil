<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





// =============== ADMIN ============================

//dashboard
Route::get('/dashboard', function () {
    if(session('berhasil_login')){
        return view('Admin.index');
    }else{
        return redirect('/login');
    }
});

//Konten
Route::get('konten', 'admin\KontenController@index')->middleware('checklogin');
Route::get('konten/kontenTambah', 'admin\KontenController@tambah')->name('konten.tambahdata');
Route::post('konten/kontenSimpan', 'admin\KontenController@simpan')->name('konten.simpandata');
Route::delete('konten/delete/{id}', 'admin\KontenController@delete')->name('konten.deletedata');
Route::get('konten/{id}/edit', 'admin\KontenController@edit')->name('konten.editdata');
Route::patch('konten/{id}', 'admin\KontenController@update')->name('konten.update');
Route::get('konten/{id}/detail', 'admin\KontenController@detail')->name('konten.detaildata');

//User
Route::get('datauser', 'admin\UserAdminController@data')->middleware('checklogin');
Route::delete('datauser/delete/{id}', 'admin\UserAdminController@delete');

//kritiksaran
Route::get('kritik', 'admin\KritikAdminController@data')->middleware('checklogin');
Route::delete('kritik/delete/{id}', 'admin\KritikAdminController@delete');

//Admin
Route::get('dataadmin', 'admin\AdminController@data')->middleware('checklogin');
Route::get('dataadmin/add', 'admin\AdminController@add');
Route::post('dataadmin', 'admin\AdminController@addProcess')->name('admin.simpandata');
Route::get('dataadmin/update/{id}', 'admin\AdminController@update')->name('admin.update');
Route::patch('dataadmin/{id}', 'admin\AdminController@updateProcess')->name('admin.updateProcess');
Route::delete('dataadmin/{id}', 'admin\AdminController@delete');

//login
Route::get('/login', 'admin\LoginController@index')->name('login');
Route::post('adminlogin', 'admin\LoginController@login')->name('login');

//logout
Route::get('logout', 'admin\LoginController@logout')->name('logout');

//register
Route::get('register', 'admin\RegisterController@index');
Route::post('register/add', 'admin\RegisterController@addProcess')->name('register');

//services
Route::get('services', 'admin\ServiceController@data')->middleware('checklogin');
Route::get('service/update/{id}', 'admin\ServiceController@update')->name('service.update');
Route::patch('service/{id}', 'admin\ServiceController@updateProcess')->name('service.updateProcess');
Route::delete('service/{id}', 'admin\ServiceController@delete')->name('service.delete');
Route::get('service/{id}/detail', 'admin\ServiceController@detail')->name('service.detaildata');

//spareparts
Route::get('spareparts', 'admin\SparepartsController@index')->middleware('checklogin');
Route::get('spareparts/sparepartsTambah', 'admin\SparepartsController@tambah')->name('spareparts.tambahdata');
Route::post('spareparts/sparepartsSimpan', 'admin\SparepartsController@simpan')->name('spareparts.simpandata');
Route::delete('spareparts/delete/{id}', 'admin\SparepartsController@delete')->name('spareparts.deletedata');
Route::get('spareparts/{id}/edit', 'admin\SparepartsController@edit')->name('spareparts.editdata');
Route::patch('spareparts/{id}', 'admin\SparepartsController@update')->name('spareparts.update');
Route::get('spareparts/{id}/detail', 'admin\SparepartsController@detail')->name('spareparts.detaildata');

//pesanan
Route::get('pesanan', 'admin\PesananController@data')->middleware('checklogin');
Route::get('pesanan/update/{id}', 'admin\PesananController@update')->name('pesanan.update');
Route::patch('pesanan/{id}', 'admin\PesananController@updateProcess')->name('pesanan.updateProcess');

//jadwal
Route::get('jadwal', 'admin\JadwalController@index')->middleware('checklogin');
Route::get('jadwal/jadwalTambah', 'admin\JadwalController@tambah')->name('jadwal.tambahdata');
Route::post('jadwal/jadwalSimpan', 'admin\JadwalController@simpan')->name('jadwal.simpandata');
Route::delete('jadwal/delete/{id}', 'admin\JadwalController@delete')->name('jadwal.deletedata');
Route::get('jadwal/{id}/edit', 'admin\JadwalController@edit')->name('jadwal.editdata');
Route::patch('jadwal/{id}', 'admin\JadwalController@update')->name('jadwal.update');

//transaksi service
Route::get('transaksi', 'admin\TransaksiController@index')->middleware('checklogin');
Route::get('transaksi/update/{id}', 'admin\TransaksiController@update')->name('transaksi.update');
Route::patch('transaksi/{id}', 'admin\TransaksiController@updateProcess')->name('transaksi.updateProcess');
Route::delete('transaksi/{id}', 'admin\TransaksiController@delete')->name('transaksi.delete');

//transaksi penjualan
Route::get('transaksiPenjualan', 'admin\TransaksiPenjualanController@index')->middleware('checklogin');


//jasa
Route::get('jasa', 'admin\JasaController@index')->middleware('checklogin');
Route::get('jasa/tambahJasa', 'admin\JasaController@tambah')->name('jasa.tambahdata');
Route::post('jasa/jasaSimpan', 'admin\JasaController@simpan')->name('Jasa.simpandata');
Route::delete('jasa/delete/{id}', 'admin\JasaController@delete')->name('Jasa.deletedata');
Route::get('jasa/{id}/edit', 'admin\JasaController@edit')->name('Jasa.editdata');
Route::patch('jasa/{id}', 'admin\JasaController@update')->name('Jasa.update');




//=============================== USER ==================================================================
Route::get('/', 'user\UserController@index' );
Route::get('user/layanan', 'user\UserController@layanan' );
Route::get('user/carakerja', 'user\UserController@caraKerja' );
Route::get('user/kontak', 'user\UserController@kontak' );
Route::post('user/add', 'user\UserController@addKS' )->name('user.addKS');

//register user
Route::get('user/register', 'user\UserRegisterController@index');
Route::post('user/register/add', 'user\UserRegisterController@addProcess')->name('register');

//login user
Route::get('user/login', 'user\UserLoginController@index')->name('login2');
Route::post('login', 'user\UserLoginController@login')->name('login');

//forgot password
Route::get('user/forgotpw', 'user\UserForgotPwController@index')->name('forgotpw');
Route::post('user/forgotpw2', 'user\UserForgotPwController@tampil')->name('forgotpw2');
Route::patch('user/forgotpw3', 'user\UserForgotPwController@forgotPw')->name('forgotpw3');


//logout user
Route::get('user/logout', 'user\UserLoginController@logout')->name('logout2');

//dashboard user
Route::get('user/utama/dashboard','user\UserHomeController@index');

//garasi
Route::get('user/utama/garasi','user\UserGarasiController@index');
Route::post('user/utama/garasisSimpan', 'user\UserGarasiController@simpan')->name('garasi.simpandata');
Route::get('user/utama/garasi/detail/{id}', 'user\UserGarasiController@detail')->name('garasi.detaildata');
Route::patch('user/utama/garasi/{id}', 'user\UserGarasiController@updateProcess')->name('garasi.updateProcess');
Route::delete('garasi/{id}', 'user\UsergarasiController@delete')->name('garasi.delete');

//pesanan
Route::get('user/utama/pesanan','user\UserPesananController@index')->name('pesanan');
Route::get('user/utama/pesananTambah', 'user\UserPesananController@tambah')->name('pesanan.tambahdata');
Route::post('user/utama/pesananSimpan', 'user\UserPesananController@simpan')->name('pesanan.simpandata');
Route::post('user/utama/pesananSimpan', 'user\UserPesananController@simpan')->name('pesanan.simpandata');

//reservasi
Route::get('user/utama/pesanan/reservasi/{kd_service}','user\UserPesananController@reservasi')->name('reservasi');

//bayar 
Route::get('user/utama/pesananTransaksi/{kd_service}', 'user\UserPesananController@data')->name('pesanan.dataTransaksi');
Route::patch('user/utama/pesananSimpanTransaksi', 'user\UserPesananController@updatedata')->name('pesanan.simpandataTransaksi');
Route::get('user/utama/checkout/{kd}', 'user\UserCheckoutController@delete')->name('deleteCheckout');

//checkout
Route::get('user/utama/checkout','user\UserCheckoutController@index')->name('checkout');
Route::get('user/utama/checkout/invoice/{nota}','user\UserCheckoutController@data')->name('invoice');
Route::get('user/utama/checkout/pdf/{nota}', 'user\UserCheckoutController@print')->name('print');


 //profile
Route::get('user/update/{id}', 'user\UserProfilController@update')->name('user.update');
Route::patch('user/{id}', 'user\UserProfilController@updateProcess')->name('user.updateProcess');
Route::patch('user/pw/{id}', 'user\UserProfilController@updatePassword')->name('user.updatePw');


//sparepart Pejualan
Route::get('user/utama/sparepart','user\UserSparepartController@index')->name('sparepart');
Route::get('user/utama/sparepart/{id}/detail', 'user\UserSparepartController@detail')->name('detail');
Route::get('user/utama/sparepart/keranjang', 'user\UserSparepartController@keranjang')->name('keranjang');
Route::post('user/utama/sparepart/keranjang', 'user\UserSparepartController@keranjangTambah')->name('keranjangTambah');
Route::get('user/utama/sparepart/keranjang/{id}', 'user\UserSparepartController@delete')->name('keranjang.delete');
Route::delete('user/utama/sparepart/keranjang/delete', 'user\UserSparepartController@deleteAll')->name('keranjang.delete2');
Route::get('user/utama/sparepart/checkout', 'user\UserSparepartController@checkout')->name('checkout');
Route::get('user/utama/sparepart/bayar', 'user\UserSparepartController@bayar')->name('bayar');
Route::get('user/utama/sparepart/hCheckout', 'user\UserSparepartController@hCheckout')->name('hCheckout');
// disini sal
Route::post('user/utama/sparepart/jsonadd', 'user\UserSparepartController@jsonPut');
Route::get('user/utama/sparepart/jsonget', 'user\UserSparepartController@jsonGet');