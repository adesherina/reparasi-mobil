<div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="index.html">Admin</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">Admin</a>
          </div>
          <ul class="sidebar-menu">
              <li class="menu-header">Dashboard</li>
              <li class="nav-item ">
                <a href="{{url('/dashboard')}}" class="nav-link "><i class="fas fa-fire"></i><span>Dashboard</span></a>
              </li>
              <li class="menu-header">Landing Page</li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-edit"></i><span>Konten</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{route('konten.tambahdata')}}">Tambah Konten</a></li>
                  <li><a class="nav-link" href="{{url('konten')}}">Lihat Konten</a></li>
                </ul>
              </li>
              <li class="nav-item ">
                <a href="{{ url('kritik')}}" class="nav-link " ><i class="fas fa-comment-alt"></i> <span>Kritik dan Saran</span></a>
              </li>
              <li class="menu-header">Pengguna</li>
              <li class="nav-item ">
                <a href="#" class="nav-link has-dropdown" ><i class="fas fa-user"></i> <span>User</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{url('dataadmin')}}">Admin</a></li>
                  <li><a class="nav-link" href="{{url('datauser')}}">Pengguna</a></li>
                </ul>
              </li>
              <li class="menu-header">Layanan Service</li>
              <li class="nav-item ">
                <a href="{{url('jasa')}}" class="nav-link "><i class="fas fa-user-friends"></i><span>Jasa</span></a>
              </li>
              <li class="nav-item ">
                <a href="{{url('services')}}" class="nav-link "><i class="fas fa-car"></i><span>Services</span></a>
              </li>
              <li class="nav-item ">
                <a href="{{url('transaksi')}}" class="nav-link "><i class="fas fa-money-check-alt"></i><span>Transaksi</span></a>
              </li>
              <li class="menu-header">Penjualan Sparepart</li>
              <li class="nav-item ">
                <a href="{{url('spareparts')}}" class="nav-link "><i class="fas fa-ring"></i><span>Sparepart</span></a>
              </li>
              <li class="nav-item ">
                <a href="{{url('transaksiPenjualan')}}" class="nav-link "><i class="fas fa-money-check-alt"></i><span>Transaksi</span></a>
              </li>


        </aside>
      </div>