@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Jasa</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Jasa</a></div>
        <div class="breadcrumb-item">Form Tambah Jasa</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('Jasa.simpandata')}}" method="POST">
        @method('post')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Tambah Jasa</h4>
              </div>
              
              <div class="card-body">
                                                        <input type="text" name="kd_jasa" value="vnbvbnv">
                <div class="form-group">
                  <label>Nama Jasa</label>
                  <input type="text" class="form-control" name="nama_jasa">
                </div>
                <div class="form-group">
                  <label>Harga Jasa</label>
                  <input type="text" class="form-control" name="harga_jasa">
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 