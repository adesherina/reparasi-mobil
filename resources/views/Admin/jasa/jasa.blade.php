@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
   <section class="section">
      <div class="section-header">
        <h1>Jasa</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Data Jasa</a></div>
          <div class="breadcrumb-item">Jasa</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Data Jasa</h4>
            <div class="card-header-action">
              <a href="#" data-toggle="modal" data-target="#exampleModal" class="btn btn-success btn-lg">
              Add
              </a>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table id="tabel-data" class="table table-bordered table-md">
                <thead>
                  <tr>
                    <th class="text-center">
                       No
                    </th>
                    <th>Nama Jasa</th>
                    <th>Harga Jasa</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach ($jasa as $jasa)
                 <tr>
                   <td class="text-center">
                     {{ $loop->iteration }}
                    </td>
                    <td>{{$jasa->nama_jasa}}</td>
                    <td>{{$jasa->harga_jasa}}</td>
                    <td>
                      <a href="{{route('Jasa.editdata', $jasa->kd_jasa)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i>
                      </a>
                      <form action="{{route('Jasa.deletedata', $jasa->kd_jasa)}}" id="delete{{$jasa->kd_jasa}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                      @method('delete')
                      <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                      @csrf
                      </form>
                    </td>
                 </tr>
                @endforeach
                </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
</section>
<div class="modal fade" tabindex="-1" role="dialog" id="exampleModal">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Tambah Jasa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="{{route('Jasa.simpandata')}}" method="POST">
                    @method('post')
                    @csrf
                    <div class="row">
                      <input type="hidden" name="kd_jasa" 
                      value="<?php function generateRandomString($length = 25) {
                         $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                         $charactersLength = strlen($characters);
                         $randomString = '';
                         for ($i = 0; $i < $length; $i++) {
                            $randomString .= $characters[rand(0, $charactersLength - 1)];
                          }
                          return $randomString;
                        }
                        echo $myRandomString = generateRandomString(4); ?>">
                        <div class="form-group col-12 col-md-12 col-lg-12">
                          <label>Nama Jasa</label>
                          <input type="text" class="form-control" name="nama_jasa">
                        </div>
                        <div class="form-group col-12 col-md-12 col-lg-12">
                          <label>Harga Jasa</label>
                          <input type="text" class="form-control" name="harga_jasa">
                        </div>
                    </div>
              </div>
              <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <input type="submit" value="Save" class="btn btn-primary" name="" id="">
              </div>
              </form>
            </div>
          </div>
        </div>
</div> 
@endsection