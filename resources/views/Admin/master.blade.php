<!DOCTYPE html>
<html lang="en">
<head>
  @include('Admin.layouts.header')
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>

        @include('Admin.layouts.navbar')
        @include('Admin.layouts.sidebar')

        <!-- Main Content -->
       @yield('content') 
       
    </div>
  </div>
  
    @include('Admin.layouts.footer')
  <!-- General JS Scripts -->
    @include('Admin.layouts.js')
</body>
</html>
