@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
        <section class="section">
          <div class="section-header">
              <h1>Service</h1>
              <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data Service</a></div>
                <div class="breadcrumb-item">Service</div>
              </div>
          </div>
          <div class="section-body">
            <!-- Alert  -->
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card card-primary">
              <div class="card-header">
                <h4>Data Service</h4>
                <div class="card-header-action">
                  <a href="#" class="btn btn-primary">
                    View All
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-md" id="tabel-data">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama user</th>
                        <th>Merk Mobil</th>
                        <th>Keluhan</th>
                        <th>Tindakan</th>
                        <th>Tanggal Service</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($services as $services)
                      <tr>
                        <td>{{$loop->iteration}}</td>  
                        <td>{{$services->name}}</td>
                        <td>{{$services->merk_kendaraan}}</td>
                        <td>{{$services->keluhan}}</td>
                        <td>@if($services->tindakan == "onload")
                          <button class="btn-sm btn-primary" disabled>{{$services->tindakan}}</button>
                          @elseif($services->tindakan == "process")
                          <button class="btn-sm btn-info" disabled>{{$services->tindakan}}</button>
                          @else
                          <button class="btn-sm btn-success" disabled>{{$services->tindakan}}</button>
                          @endif
                        </td>
                        <td>{{$services->tanggal_service}}</td>
                        <td>
                          <a href="{{route('service.update', $services->kd_service)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i></a>
                          <form action="{{url('service/'. $services->kd_service)}}"  onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                            @method('delete')
                            <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                            @csrf
                          </form>
                          <a href="{{route('service.detaildata', $services->kd_service)}}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-eye"></i></a>
                        </td>
                      </tr>    
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div> 
        </section>
      </div> 
@endsection
 