@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
  <section class="section">
      <div class="section-header">
        <h1>Detail Service</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Detail Service</a></div>
          <div class="breadcrumb-item">Service</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Detail Service</h4>
            <div class="card-header-action">
              <a href="{{url('services')}}" class="btn btn-success btn-lg">
              kembali
            </a>
            </div>
          </div>

          <div class="col-12 mt-3 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Nama Pemilik : {{$services->name}}</h4>
              </div>
              <div class="card-body">
                <p class="card-text text-primary">Merk Mobil : {{$services->merk_kendaraan}}</p>
              </div>
              <div class="card-body">
                <p class="card-text text-primary">Jenis Service : <?php echo $services->jenis_service ?></p>
              </div>
              <div class="card-body">
                <p class="card-text">Keluhan : {{$services->keluhan}}</p>
              </div>
              <div class="card-body">
                <p class="card-text text-primary">Tindakan : {{$services->tindakan}}</p>
              </div>
              <div class="card-body">
                <p class="card-text text-primary">Tanggal Service : {{$services->tanggal_service}}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
</div> 
@endsection