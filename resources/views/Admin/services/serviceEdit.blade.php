@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Service</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Service</a></div>
        <div class="breadcrumb-item">Form Update Service</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('service.updateProcess', $services->kd_service)}}" method="POST">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Edit Konten</h4>
                <div class="card-header-action">
                  <a href="{{url('services')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama user</label>
                  <input type="text" class="form-control" name="name" value="{{$services->name}}" disabled>
                </div>
                <div class="form-group">
                  <label>Merk Kendaraan</label>
                  <input type="text" class="form-control" name="merk_kendaraan" value="{{$services->merk_kendaraan}}" disabled>
                </div>
                <div class="form-group">
                  <label>Jenis Service</label>
                  <input type="text" class="form-control" name="jenis_service" value="{{$services->jenis_service}}" disabled>
                </div>
                <div class="form-group">
                  <label>Keluhan</label>
                  <input type="text" class="form-control" name="keluhan" value="{{$services->keluhan}}" disabled>
                </div>
                <div class="form-group" >
                  <label>Tindakan</label>
                  <select class="form-control" name="tindakan" >
                    <option value="{{$services->tindakan}}" selected>{{$services->tindakan}}</option>
                    <option value="OnLoad">OnLoad</option>
                    <option value="Process">Process</option>
                    <option value="Success">Success</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Tanggal Service</label>
                  <input type="text" class="form-control" name="tanggal_service" value="{{$services->tanggal_service}}" disabled>
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>   
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 