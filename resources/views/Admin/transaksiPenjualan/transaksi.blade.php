@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
  <section class="section">
      <div class="section-header">
        <h1>Transaksi Penjualan</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Data Transaksi Penjualan</a></div>
          <div class="breadcrumb-item">Transaksi Penjualan</div>
        </div>
      </div>
      <div class="section-body">
          <!-- Alert Tambah Data -->
          @if (session('status'))
          <div class="alert alert-success">
            {{ session('status') }}
          </div>
          @endif
          <div class="card card-primary">
              <div class="card-header">
                <h4>Data Transaksi Penjualan</h4>
                <div class="card-header-action">
                  <a href="#" class="btn btn-success btn-lg">
                  View All
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="tabel-data" class="table table-bordered table-md">
                    <thead>
                      <tr>
                        <th class="text-center">
                          No
                        </th>
                        <th>Nota</th>
                        <th>Nama Pengguna</th>
                        <th>Total Barang</th>
                        <th>Tanggal Transaksi</th>
                        <th>Bukti Transfer</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                       <tr>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                       </tr>
                      </tbody>
                    </table>
                  </div>
              </div>
        </div>
      </div>
  </section>
</div>
    
@endsection