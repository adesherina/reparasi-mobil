@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
   <section class="section">
      <div class="section-header">
        <h1>Jadwal</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Data Jadwal</a></div>
          <div class="breadcrumb-item">Jadwal</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Data Jadwal</h4>
            <div class="card-header-action">
              <a href="#" data-toggle="modal" data-target="#exampleModal" class="btn btn-success btn-lg">
              Add
              </a>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered table-md" id="tabel-data">
                <thead>
                  <tr>
                    <th class="text-center">
                       No
                    </th>
                    <th>Nama Pelanggan</th>
                    <th>Jam Mulai</th>
                    <th>Jam Selesai</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach ($jadwal as $jadwal)
                 <tr>
                   <td class="text-center">
                     {{ $loop->iteration }}
                    </td>
                    <td>{{$jadwal->name}}</td>
                    <td>{{$jadwal->jam_mulai}}</td>
                    <td>{{$jadwal->jam_selesai}}</td>
                    <td>
                      <a href="{{route('jadwal.editdata', $jadwal->id_jadwal)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i>
                    </a>
                    <form action="{{route('jadwal.deletedata', $jadwal->id_jadwal)}}" id="delete{{$jadwal->id_jadwal}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                    @method('delete')
                    <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                    @csrf
                  </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="exampleModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Jadwal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('jadwal.simpandata')}}" method="POST">
                @method('post')
                @csrf
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="form-group" >
                            <label>Nama Pelanggan</label>
                            <select class="form-control" name ="nota" value="">
                                <?php 
                                    $service = DB::table('services')
                                        ->join('users', 'users.id', '=', 'services.user_id')
                                        ->get();
                                ?> 
                            @foreach($service as $item)
                                <option value="{{$item->nota}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                        </div>
                        <div class="form-group">
                            <label>Jam Mulai</label>
                            <input type="time" class="form-control" name="jam_mulai">
                        </div>
                        <div class="form-group">
                            <label>Jam Selesai</label>
                            <input type="time" class="form-control" name="jam_selesai">
                        </div> 
                </div> 
                </div>
            </form>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
</section>
</div> 
@endsection
