@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Jadwal</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Jadwal</a></div>
        <div class="breadcrumb-item">Form Tambah Jadwal</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('jadwal.simpandata')}}" method="POST">
        @method('post')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Tambah Jadwal</h4>
              </div>
              <div class="card-body">
                <div class="form-group" >
                  <label>Nama Pelanggan</label>
                  <select class="form-control" name ="nota" value="">
                     <?php 
                        $service = DB::table('services')
                            ->join('users', 'users.id', '=', 'services.user_id')
                            ->get();
                     ?> 
                     @foreach($service as $item)
                        <option value="{{$item->nota}}">{{$item->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label>Jam Mulai</label>
                  <input type="text" class="form-control" name="jam_mulai">
                </div>
                <div class="form-group">
                  <label>Jam Selesai</label>
                  <input type="text" class="form-control" name="jam_selesai">
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 