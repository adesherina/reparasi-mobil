@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Konten</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Konten</a></div>
        <div class="breadcrumb-item">Form Update Konten</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('konten.update', $konten->id)}}" method="POST">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Edit Konten</h4>
                <div class="card-header-action">
                  <a href="{{url('konten')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Judul Konten</label>
                  <input type="text" class="form-control" name="judul_konten" value="{{$konten->judul_konten}}">
                </div>
                <div class="form-group"> 
                  <label>Isi Konten</label>
                 <textarea class="form-control" name="isi_konten" >{{$konten->isi_konten}}</textarea>
                </div>
                <div class="form-group" >
                  <label>Kategori</label>
                  <select class="form-control" name="kategori" >
                    <option value="{{$konten->kategori}}" selected>{{$konten->kategori}}</option>
                    <option value="Home">Home</option>
                    <option value="Layanan">Layanan</option>
                    <option value="Cara Kerja">Cara Kerja</option>
                    <option value="Kontak">Kontak</option>
                  </select>
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 