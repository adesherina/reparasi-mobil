@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Konten</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Konten</a></div>
        <div class="breadcrumb-item">Form Tambah</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('konten.simpandata')}}" method="POST">
        @method('post')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Tambah Konten</h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Judul Konten</label>
                  <input type="text" class="form-control" name="judul_konten">
                </div>
                <div class="form-group"> 
                  <label>Isi Konten</label>
                 <textarea class="form-control" name="isi_konten"></textarea>
                </div>
                <div class="form-group" >
                  <label>Kategori</label>
                  <select class="form-control" name = "kategori">
                    <option>Home</option>
                    <option>Layanan</option>
                    <option>Cara Kerja</option>
                    <option>Kontak</option>
                  </select>
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 