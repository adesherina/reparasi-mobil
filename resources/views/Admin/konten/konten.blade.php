@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
   <section class="section">
      <div class="section-header">
        <h1>Konten</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Data Konten</a></div>
          <div class="breadcrumb-item">Konten</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Data Admin</h4>
            <div class="card-header-action">
              <a href="konten/kontenTambah" class="btn btn-success btn-lg">
              Add
              </a>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered table-md" id="tabel-data">
                <thead>
                  <tr>
                    <th class="text-center">
                       No
                    </th>
                    <th>Judul Konten</th>
                    <th>Isi Konten</th>
                    <th>Kategori</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach ($konten as $konten)
                 <tr>
                   <td class="text-center">
                     {{ $loop->iteration }}
                    </td>
                    <td>{{$konten->judul_konten}}</td>
                    <td>{{ substr(strip_tags($konten->isi_konten), 0, 102) }}</td>
                    <td>{{$konten->kategori}}</td>
                    <td>
                      <a href="{{route('konten.editdata', $konten->id)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i>
                    </a>
                    <form action="{{route('konten.deletedata', $konten->id)}}" id="delete{{$konten->id}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                    @method('delete')
                    <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                    @csrf
                  </form>
                  <a href="{{route('konten.detaildata', $konten->id)}}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-eye"></i></a>

              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
</section>
</div> 
@endsection

<!-- @push('page-scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endpush

@push('after-script')
<script>
  $(".confirm").click(function(e) {
    id= e.target.dataset.id;
    swal({
        title: 'Yakin hapus data',
        text: 'Data yang sudah dihapus tidak bisa dikembalikan!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
        $(`#delete{id}`).submit();
        }
      });
  });
</script>
@endpush -->
 