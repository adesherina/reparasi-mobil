@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
  <section class="section">
      <div class="section-header">
        <h1>Detail Konten</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Detail Konten</a></div>
          <div class="breadcrumb-item">Konten</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Detail Konten</h4>
            <div class="card-header-action">
              <a href="{{url('konten')}}" class="btn btn-success btn-lg">
              kembali
            </a>
            </div>
          </div>

          <div class="col-12 mt-3 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">{{$konten->judul_konten}}</h4>
              </div>
              <div class="card-body">
                <p class="card-text text-primary">Kategori : {{$konten->kategori}}</p>
              </div>
              <div class="card-body">
                <p class="card-text">{{$konten->isi_konten}}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
</div> 
@endsection