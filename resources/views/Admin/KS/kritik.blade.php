@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
        <section class="section">
          <div class="section-header">
              <h1>Kritik dan Saran</h1>
              <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data Kritik dan Saran</a></div>
                <div class="breadcrumb-item">Kritik dan Saran</div>
              </div>
          </div>
          <div class="section-body">
            <!-- Alert  -->
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card card-primary">
              <div class="card-header">
                <h4>Data Kritik dan Saran</h4>
                <div class="card-header-action">
                  <a href="#" class="btn btn-primary">
                    View All
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive" >
                  <table id="tabel-data" class="table table-bordered table-md">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama User</th>
                        <th>Isi</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($kritiksaran as $KS)
                      <tr>
                        <td>{{$loop->iteration}}</td>  
                        <td>{{$KS->nama}}</td>
                        <td>{{$KS->isi}}</td>
                        <td>
                        <form action="{{url('KS/delete', $KS->id)}}" id="delete{{$KS->id}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                            @method('delete')
                            <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                            @csrf
                        </form></td>
                      </tr>    
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div> 
        </section>
      </div> 
@endsection
 