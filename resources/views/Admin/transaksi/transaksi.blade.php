@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
  <section class="section">
      <div class="section-header">
        <h1>Transaksi Service</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Data Transaksi Service</a></div>
          <div class="breadcrumb-item">Transaksi Service</div>
        </div>
      </div>
      <div class="section-body">
          <!-- Alert Tambah Data -->
          @if (session('status'))
          <div class="alert alert-success">
            {{ session('status') }}
          </div>
          @endif
          <div class="card card-primary">
              <div class="card-header">
                <h4>Data Transaksi Service</h4>
                <div class="card-header-action">
                  <a href="spareparts/sparepartsTambah" class="btn btn-success btn-lg">
                  View All
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="tabel-data" class="table table-bordered table-md">
                    <thead>
                      <tr>
                        <th class="text-center">
                          No
                        </th>
                        <th>Nota</th>
                        <th>Nama Pengguna</th>
                        <th>Total Harga</th>
                        <th>Tanggal Transaksi</th>
                        <th>Bukti Transfer</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($ts as $ts)
                          <tr>
                            <td>{{$loop->iteration}}</td>  
                            <td>{{$ts->nota}}</td>
                            <td>{{$ts->name}}</td>
                            <td>{{$ts->total_harga}}</td>
                            <td>{{$ts->tgl_transaksi}}</td>
                            <td>{{$ts->file}}</td>
                            <td>
                              @if($ts->status == "unpaid")
                              <button class="btn-sm btn-primary" disabled>{{$ts->status}}</button>
                              @elseif($ts->status == "unverified")
                              <button class="btn-sm btn-info" disabled>{{$ts->status}}</button>
                              @else
                              <button class="btn-sm btn-success" disabled>{{$ts->status}}</button>
                              @endif
                            </td>
                            <td>
                              <a href="{{route('transaksi.update', $ts->nota)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i></a>
                              <form action="{{url('transaksi/'. $ts->nota)}}"  onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                                @method('delete')
                                <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                                @csrf
                              </form>
                            </td>
                          </tr>    
                        @endforeach
                      </tbody>
                    </table>
                  </div>
              </div>
        </div>
      </div>
  </section>
</div>
    
@endsection