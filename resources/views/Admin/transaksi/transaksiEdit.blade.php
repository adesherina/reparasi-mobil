@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Transaksi</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Transaksi</a></div>
        <div class="breadcrumb-item">Form Update Transaksi</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('transaksi.updateProcess', $ts->nota)}}" method="POST">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Edit Konten</h4>
                <div class="card-header-action">
                  <a href="{{url('transaksi')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nota</label>
                  <input type="text" class="form-control" name="nota" value="{{$ts->nota}}" disabled>
                </div>
                <div class="form-group">
                  <label>Nama user</label>
                  <input type="text" class="form-control" name="name" value="{{$ts->name}}" disabled>
                </div>
                <div class="form-group">
                  <label>Total Harga</label>
                  <input type="text" class="form-control" name="total_harga" value="{{$ts->total_harga}}" disabled>
                </div>
                <div class="form-group">
                  <label>Tanggal Transaksi</label>
                  <input type="text" class="form-control" name="tgl_transaksi" value="{{$ts->tgl_transaksi}}" disabled>
                </div>
                <div class="form-group">
                  <label>Bukti Transfer</label>
                  <input type="text" class="form-control" name="file" value="{{$ts->file}}" disabled>
                </div>
                <div class="form-group" >
                  <label>Status</label>
                  <select class="form-control" name="status" >
                    <option value="{{$ts->status}}" selected>{{$ts->status}}</option>
                    <option value="unpaid">unpaid</option>
                    <option value="unverified">unverified</option>
                    <option value="verified">verified</option>
                  </select>
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>   
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 