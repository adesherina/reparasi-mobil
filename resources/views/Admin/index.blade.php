@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Dashboard</h1>
    </div>
    
    <div class="section-body">
      <div class="container-fluid"> 
        <div class="row">
          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-warning">
                <i class="far fa-edit"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  use Illuminate\Support\Facades\DB;
                  $konten = DB::table('konten')->get();
                  $count = $konten->count();
                ?>
                  <div class="card-header">
                    <h4>Total Konten</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-danger">
                <i class="far fa-comment-alt"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $ks = DB::table('kritiksaran')->get();
                  $count = $ks->count();
                ?>
                  <div class="card-header">
                    <h4>Total Kritik dan Saran</h4>
                  </div>
                  <div class="card-body">
                   {{$count}}
                  </div>
              </div>
            </div>
          </div>
          
          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-primary">
                <i class="far fa-user"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $admin = DB::table('admin')->get();
                  $count = $admin->count();
                ?>
                  <div class="card-header">
                    <h4>Total Admin</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-info">
                <i class="fas fa-user-friends"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $users = DB::table('users')->get();
                  $count = $users->count();
                ?>
                  <div class="card-header">
                    <h4>Total Pengguna</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-success">
                <i class="fas fa-user-friends"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $jasa = DB::table('jasa')->get();
                  $count = $jasa->count();
                ?>
                  <div class="card-header">
                    <h4>Total Jasa</h4>
                  </div>
                  <div class="card-body">
                    {{{$count}}}
                  </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-info">
                <i class="fas fa-car"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $sc = DB::table('services')->get();
                  $count = $sc->count();
                ?>
                  <div class="card-header">
                    <h4>Total Service</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-warning">
                <i class="fas fa-money-check-alt"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $ts = DB::table('transaksi_service')->get();
                  $count = $ts->count();
                ?>
                  <div class="card-header">
                    <h4>Total Transaksi Service</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-danger">
                <i class="fas fa-money-check-alt"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $sp = DB::table('spareparts')->get();
                  $count = $sp->count();
                ?>
                  <div class="card-header">
                    <h4>Total Sparepart</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-primary">
                <i class="fas fa-money-check-alt"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $dp = DB::table('detail_penjualan')->get();
                  $count = $dp->count();
                ?>
                  <div class="card-header">
                    <h4>Total Transaksi Penjualan Sparepart</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>

        </div> 
      </div>
    </div>
  </section>
</div> 
@endsection
 