@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Sparepart</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Sparepart</a></div>
        <div class="breadcrumb-item">Form Update Sparepart</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('spareparts.update', $spareparts->id_sparepart)}}" method="POST">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Edit Sparepart</h4>
                <div class="card-header-action">
                  <a href="{{url('spareparts')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
                <div class="card-body">
                    <div class="form-group">
                      <label>Merk Sparepart</label>
                      <input type="text" class="form-control" name="merk" value="{{$spareparts->merk}}">
                    </div>
                    <div class="form-group">
                      <label>Nama Sparepart</label>
                      <input type="text" class="form-control" name="nama_sparepart" value="{{$spareparts->nama_sparepart}}">
                    </div>
                    <div class="form-group">
                      <label>Harga Jual</label>
                      <input type="text" class="form-control" name="harga_jual" value="{{$spareparts->harga_jual}}">
                    </div>
                    <div class="form-group">
                      <label>Harga Jual</label>
                      <input type="text" class="form-control" name="harga_beli" value="{{$spareparts->harga_beli}}">
                    </div>
                    <div class="form-group">
                      <label>Stok</label>
                      <input type="text" class="form-control" name="stok" value="{{$spareparts->stok}}">
                    </div>
                    <div class="form-group"> 
                      <label>Deskripsi</label>
                    <textarea class="form-control" name="deskripsi">{{$spareparts->deskripsi}}</textarea>
                    </div>
                    <div class="form-group">
                      <label>Gambar</label>
                      <input type="file" class="form-control" name="gambar" value="{{$spareparts->gambar}}">
                    </div>
                    <div class="card-footer text-right">
                      <button class="btn btn-primary mr-1" type="submit">Submit</button>
                    </div>
                </div>
            </div> 
          </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 