@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
   <section class="section">
      <div class="section-header">
        <h1>Sparepart</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Data Sparepart</a></div>
          <div class="breadcrumb-item">Sparepart</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Data Sparepart</h4>
            <div class="card-header-action">
              <a href="spareparts/sparepartsTambah" class="btn btn-success btn-lg">
              Add
              </a>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table id="tabel-data" class="table table-bordered table-md">
                <thead>
                  <tr>
                    <th class="text-center">
                       No
                    </th>
                    <th>Nama sparepart</th>
                    <th>Harga Jual</th>
                    <th>Harga Beli</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach ($spareparts as $spr)
                 <tr>
                   <td class="text-center">
                     {{ $loop->iteration }}
                    </td>
                    <td>{{$spr->nama_sparepart}}</td>
                    <td>{{$spr->harga_jual}}</td>
                    <td>{{$spr->harga_beli}}</td>
                    <td>
                      <a href="{{route('spareparts.editdata', $spr->id_sparepart)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i>
                      </a>
                      <form action="{{route('spareparts.deletedata', $spr->id_sparepart)}}" id="delete{{$spr->id_sparepart}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                      @method('delete')
                      <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                      @csrf
                      </form>
                      <a href="{{route('spareparts.detaildata', $spr->id_sparepart)}}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-eye"></i></a>
                    </td>
                 </tr>
                @endforeach
                </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
</section>
</div> 
@endsection