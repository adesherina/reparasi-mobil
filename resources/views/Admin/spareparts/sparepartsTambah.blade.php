@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Sparepart</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Sparepart</a></div>
        <div class="breadcrumb-item">Form Tambah</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('spareparts.simpandata')}}" method="POST" enctype="multipart/form-data">
        @method('post')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Tambah Sparepart</h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Merk Sparepart</label>
                  <input type="text" class="form-control" name="merk">
                </div>
                <div class="form-group">
                  <label>Nama Sparepart</label>
                  <input type="text" class="form-control" name="nama_sparepart">
                </div>
                <div class="form-group">
                  <label>Harga Jual</label>
                  <input type="text" class="form-control" name="harga_jual">
                </div>
                <div class="form-group">
                  <label>Harga Beli</label>
                  <input type="text" class="form-control" name="harga_beli">
                </div>
                <div class="form-group">
                  <label>Stok</label>
                  <input type="text" class="form-control" name="stok">
                </div>
                <div class="form-group"> 
                  <label>Deskripsi</label>
                 <textarea class="form-control" name="deskripsi"></textarea>
                </div>
                <div class="form-group">
                  <label>Gambar</label>
                  <input type="file" class="form-control" name="gambar">
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 