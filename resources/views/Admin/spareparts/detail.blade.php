@extends('Admin.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="main-content">
  <section class="section">
      <div class="section-header">
        <h1>Detail Sparepart</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Detail Sparepart</a></div>
          <div class="breadcrumb-item">Sparepart</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Detail Sparepart</h4>
            <div class="card-header-action">
              <a href="{{url('spareparts')}}" class="btn btn-success btn-lg">
              kembali
            </a>
            </div>
          </div>

            <div class="col-12 mt-3 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Nama Sparepart : {{$spr->nama_sparepart}}</h4>
                    </div>
                    <div class="card-body">
                        <p class="card-text text-primary">Merk : {{$spr->merk}}</p>
                    </div>
                    <div class="card-body">
                        <p class="card-text text-primary">Harga Jual : {{$spr->harga_jual}}</p>
                    </div>
                    <div class="card-body">
                        <p class="card-text text-primary">Harga Beli : {{$spr->harga_beli}}</p>
                    </div>
                    <div class="card-body">
                        <p class="card-text text-primary">Stok : {{$spr->stok}}</p>
                    </div>
                    <div class="card-body">
                        <p class="card-text text-primary">Deskripsi :</p>
                        <p class="card-text text-primariy"> {{$spr->deskripsi}}</p>
                    </div>
                    <div class="card-body">
                        <p class="card-text text-primary">Gambar :</p>
                        <img src="{{url('/gambar/'.$spr->gambar)}}" style="width: 200px;">
                    </div>
                </div>
            </div>

        </div>
      </div>
  </section>
</div> 
@endsection