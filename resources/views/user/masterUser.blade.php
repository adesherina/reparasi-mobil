<!DOCTYPE html>
<html lang="en">
<head>
@include('user.layouts.landingpage.head')
</head>
<body data-spy="scroll" data-target="#navbarNav" data-offset="50">

    <!-- NAVBAR -->
    @include('user.layouts.landingpage.navbar')

     <!-- HERO -->
     <!-- Main Content -->
     <section class="hero d-flex flex-column justify-content-center align-items-center" id="home">
            <div class="bg-overlay"></div>
                @yield('content')
    </section>

    <!-- FOOTER -->
    @include('user.layouts.landingpage.footer') 

     <!-- SCRIPTS -->
     @include('user.layouts.landingpage.js')

</body>
</html>