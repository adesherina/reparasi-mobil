@extends('user.masterUser')
@section('title', 'Reparasi Mobil')


@section('content')
<section class="about section" id="about" style="margin-top: 200px !important;">
    <div class="container" style="padding-top: -200px !important;">
      <div class="row">
          <div class="col-md-8">
              <article itemscope itemtype="http://schema.org/Article">
              <header>
                  <h1 class="text-white">Mobil bermasalah? Kami siap membantu</h1>
                </header>
                <content class="text-white">
                    <p class="text-white">Jadwalkan servis mobil Anda online dan mekanik kami akan datang dimanapun Anda berada. Semua prosesnya sangat mudah. Dengan menggunakan website kami, Anda bisa mengatur jadwal servis, mendapatkan penawaran harga, dan melihat riwayat servis mobil Anda. Selain itu harga servis kami juga lebih rendah dari harga bengkel karena kami tidak mempunyai bengkel fisik sehingga biaya operasional kami lebih rendah.</p>
                    <p>&nbsp;</p>
                    <h1>Booking servis Anda dengan mudah</h1>
                    <div class="row" style="margin-top: 40px;">
                    <div class="col-md-1"><img class="img-center img-responsive" src="{{url('assets/user/images/car.png')}}" width="30px" /></div>
                    <div class="col-md-11">
                        <h3>JELASKAN KEBUTUHAN ANDA</h3>
                        <ol>
                            <li>Masukan informasi mobil Anda bedasarkan merek, tahun, dan tipe</li>
                            <li>Pilih jenis servis dan sparepart yang Anda butuhkan</li>
                            <li>Berikan informasi Anda (nama, lokasi, nomor telpon, dll)</li>
                        </ol>
                    </div>
                </div>
                <p>&nbsp;</p>
                <div class="row">
                    <div class="col-md-1"><img class="img-center img-responsive" src="{{url('assets/user/images/document.png')}}" width="30px"/></div>
                    <div class="col-md-11">
                        <h3>DAPATKAN HARGA SERVIS YANG BERSAING&nbsp;</h3>
                        <p class="text-white">Kami akan memastikan Anda mendapatkan harga servis yang terbaik dan transparan. Harga dari jasa dan produk akan dijabarkan secara lengkap di penawaran harga kami. Anda juga bisa menghemat karena harga servis kami lebih murah sampai dengan 30% dari harga bengkel.</p>
                    </div>
                </div>
            </content>
        </article>
</div>
</div>
</div>
</div>
</section>

@endsection