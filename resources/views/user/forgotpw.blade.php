@extends('user.masterUser')
@section('title', 'Reparasi Mobil')


@section('content')
    <div class="container px-5 py-5" style="margin-top: 120px;margin-bottom:100px; background-color:#17181a;">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                    <img src="{{url('assets/img/bengkel.jpg')}}" width="600px" class="img-fluid" alt="Responsive image">  
            </div>
            <div class="ml-auto col-lg-6 col-md-6 col-12" >
                <!-- Alert  -->
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @elseif (session('status'))
                        <div class="alert alert-danger">
                            {{ session('filed') }}
                        </div>
                    @endif
                <h2 class="mb-4 pb-2 text-white" data-aos="fade-up" data-aos-delay="200">Konfirmasi Nomor Telepon</h2>
                <form action="{{route('forgotpw2')}}" method="post" class="contact-form webform" data-aos="fade-up" data-aos-delay="400" role="form">
                    @csrf
                    @method('post')
                    <input type="text" class="form-control" name="no_telp" placeholder="Masukan No Telpon Anda">
                    <button class="form-control btn btn-success">Lanjut</button>
                </form>
            </div>
        </div>
    </div>
@endsection