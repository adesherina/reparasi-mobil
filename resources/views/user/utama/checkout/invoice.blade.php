@extends('user.utama.master')
@section('title', 'Reparasi Mobil')


@section('content')

<div class="container">
    <div class="row">
        <a href="{{route('print',$ts->nota)}}" class="btn btn-sm btn-danger"> Print</a>
    </div>

    <div class="row">
        <div class="offset-xl-2 col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header p-4">
                    <h3 class="pt-2 d-inline-block">Bengkel Mobil Tiga Saudara</h3>
                    <div class="float-right"> <p class="mb-0">Nota : {{$ts->nota}}</p>
                        Tanggal : {{$ts->tgl_transaksi}}
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive-sm">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Jenis Service</th>
                                    <th>Harga Sparepart</th>
                                    <th class="right">Jasa</th>
                                    <th class="right">Total Harga</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="left strong">{{$ts->jenis_service}}</td>
                                    <td class="left">Rp. {{$ts->harga_sparepart}}</td>
                                    <td class="right">Rp. {{$ts->harga_service}}</td>
                                    <?php $total_harga = $ts->harga_service+$ts->harga_sparepart ?>
                                    <td class="center">Rp. {{$total_harga}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-sm-5">
                        </div>
                        <div class="col-lg-4 col-sm-5 ml-auto">
                            <table class="table table-clear">
                                <tbody>
                                    <tr>
                                        <td class="left">
                                            <strong class="text-dark">Total</strong>
                                        </td>
                                        <td class="right">
                                            <?php $total_harga = $ts->harga_service+$ts->harga_sparepart ?>
                                            <strong class="text-dark">{{$total_harga}}</strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card-footer bg-white">
                    <p class="mb-0">Cek Terlebih Dahulu Mobil Anda Sebelum Pergi dari Bengkel</p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
