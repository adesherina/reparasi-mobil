<!DOCTYPE html>
<html lang="en">
<head>
    <!-- <link rel="stylesheet" href="{{url('assets/bootstrap/css/bootstrap.min.css')}}"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>

<style>
.invoice-title h2, .invoice-title h3 {
    display: inline-block;
}

.table > tbody > tr > .no-line {
    border-top: none;
}

.table > thead > tr > .no-line {
    border-bottom: none;
}

.table > tbody > tr > .thick-line {
    border-top: 2px solid;
}
</style>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="invoice-title">
                    <h2>Bengkel Mobil Tiga Saudara</h2>
                </div>
                
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="invoice-title2 " >
                    <p class="text-right">Nota : {{$ts->nota}}</p>
                    
                </div>
                <div style="margin-top: -10px;">
                    <p class="text-right">Tanggal : {{$ts->tgl_transaksi}}</p>
                </div>
            </div>
        </div>
        
        <div class="row mt-2">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <p class="panel-title"><strong>Pemesanan</strong></p>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                    <tr>
                                        <td class="text-center"><strong>Jenis Service</strong></td>
                                        <td class="text-center"><strong>Harga Sparepart</strong></td>
                                        <td class="text-center"><strong>Jasa</strong></td>
                                        <td class="text-right"><strong>Total</strong></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">{{$ts->jenis_service}}</td>
                                        <td class="text-center">Rp. {{$ts->harga_sparepart}}</td>
                                        <td class="text-center">Rp. {{$ts->harga_service}}</td>
                                        <?php $total_harga = $ts->harga_service+$ts->harga_sparepart ?>
                                        <td class="text-right">Rp. {{$total_harga}}</td>
                                    </tr>
                                    <tr>
    								<td class="thick-line"></td>
    								<td class="thick-line"></td>
    								<td class="thick-line text-center"><strong>Total</strong></td>
    								<td class="thick-line text-right">Rp. {{$total_harga}}</td>
    							</tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <script src="{{url('assets/bootstrap/js/bootstrap.min.js')}}"></script> -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>
</html>