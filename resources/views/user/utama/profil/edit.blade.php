@extends('user.utama.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Profil</h2>
            <p class="pageheader-text"></p>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Profil</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Profil Anda</li>
                    </ol>
                </nav>
            </div>
                <!-- Alert Tambah Data -->
                    @if (session('status'))
                    <div class="alert alert-success mt-3">
                    {{ session('status') }}
                    </div>
                    @endif
            <div class="row">
                <div class="card col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
                    <h5 class="card-header">Profil</h5>
                        <div class="card-body">
                            <form action="{{route('user.updateProcess', $user->id)}}" method="POST" enctype="multipart/form-data">
                                @method('patch')
                                @csrf
                                <div class="row">
                                    <div class="offset-xl-3 col-xl-6 offset-lg-3 col-lg-3 col-md-12 col-sm-12 col-12 p-4">
                                        <div class="form-group">
                                            <label for="username">Username</label>
                                            <div class="input-group ">
                                                <span class="input-group-prepend"><span class="input-group-text">@</span></span>
                                                <input type="text" placeholder="Username" name="username" class="form-control" value="{{$user->name}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Nama Lengkap</label>
                                            <input type="text" class="form-control form-control-lg" name="name" value="{{$user->name}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Nomor Telepon</label>
                                            <input type="text" class="form-control form-control-lg" name="no_telp" value="{{$user->no_telp}}">
                                        </div>
                                        <h5>Jenis Kelamin</h5>
                                            <label class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" name="jenis_kelamin" class="custom-control-input" <?php if($user->jenis_kelamin == "Laki-laki"){ ?> checked <?php } ?>  value="Laki-laki"><span class="custom-control-label">Laki - Laki</span>
                                            </label>
                                            <label class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" name="jenis_kelamin" class="custom-control-input" <?php if($user->jenis_kelamin == "Perempuan"){ ?> checked <?php } ?>  value="Perempuan"><span class="custom-control-label">Perempuan</span>
                                            </label>
                                        <div class="form-group">
                                            <label for="messages">Alamat</label>
                                            <textarea class="form-control" name="alamat" rows="3">{{$user->alamat}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="messages">Kode Pos</label>
                                            <input type="text" class="form-control form-control-lg" name="kode_pos" value="{{$user->kode_pos}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="messages">Upload Foto</label>
                                            <input type="file" class="form-control form-control-lg" name="foto">
                                        </div>
                                        <button type="submit" class="btn btn-primary float-right">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
            <!-- Alert Tambah Data -->
                    @if (session('editpw'))
                    <div class="alert alert-success mt-3">
                    {{ session('editpw') }}
                    </div>
                    @elseif(session('failedpw'))
                    <div class="alert alert-danger mt-3">
                    {{ session('failedpw') }}
                    </div>
                    @endif
            <div class="row">
                <div class="card col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
                    <h5 class="card-header">Ganti Password</h5>
                        <div class="card-body">
                            <form action="{{url('user/pw', $user->id)}}" method="POST" enctype="multipart/form-data">
                                @method('patch')
                                @csrf
                                <div class="row">
                                    <div class="offset-xl-3 col-xl-6 offset-lg-3 col-lg-3 col-md-12 col-sm-12 col-12 p-4">
                                        
                                        <div class="form-group">
                                            <label for="pwbaru">Password Baru</label>
                                            <input type="password" class="form-control form-control-lg" name="newPassword" >
                                        </div>
                                        <div class="form-group">
                                            <label for="ulangpw">Konfirmasi Password</label> 
                                            <input type="password" class="form-control form-control-lg" name="confirmPassword">
                                        </div>
                                        <button type="submit" class="btn btn-primary float-right">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection