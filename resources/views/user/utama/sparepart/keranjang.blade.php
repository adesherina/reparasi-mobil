@extends('user.utama.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Keranjang</h2>
                <p class="pageheader-text"></p>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Keranjang</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Keranjang Anda</li>
                        </ol>
                    </nav>

                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="tabel-data penjualan" class="table">
                                        <thead>
                                        <tr>
                                            <th>
                                               Pilih
                                            </th>
                                            <th>
                                            Produk
                                            </th>
                                            <th>Harga</th>
                                            <th >Qty</th>
                                            <th >Total Harga</th>
                                            <th >Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($keranjang as $keranjang)
                                            <tr class="penjualan"  id="tr_{{$keranjang->id_keranjang}}">
                                                <td id="90"><input type="checkbox" id="60" price="20" name="id_keranjang[]" price="2" class="checkedItem" data-harga="<?= $keranjang->harga; ?>" data-qty="<?= $keranjang->jumlah ?>" data-id="<?= $keranjang->id_keranjang?>" data-nama="<?$keranjang->nama_sparepart?>" data-subtotal="<?$keranjang->subtotal?>"></td>
                                                <td>{{$keranjang->nama_sparepart}}</td>
                                                <td id="23442"><p>{{$keranjang->harga}}</p></td>
                                                <td id="234">
                                                    <input type="number" name="qty"  class="qty form-control qty2"  min="1" max="100" step="1" value="{{$keranjang->jumlah}}">
                                                </td>
                                                <td ><p class="subtotal">{{$keranjang->subtotal}}</p></td>
                                                <td>
                                                    <a href="{{route('keranjang.delete', $keranjang->id_keranjang)}}" class="btn btn-danger deleteKeranjang"><i class="fas fa-trash-alt"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3" id="body-checkout">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="tabel-data subtotal" class="table">
                                        <thead>
                                        <tr>
                                            <th><input type="checkbox" id="checkAll" ></th>
                                            <th>
                                            Pilih Semua
                                            </th>
                                            <th><a href="#" id="deleteAll" data-url="{{ route('keranjang.delete2') }}" data-csrf="{{Session::token()}}"> Hapus</a></th>
                                            
                                            <th >Total Harga : <span id="total"></span></th>
                                            <th>
                                                <a href="{{route('checkout')}}" class="btn btn-primary">Checkout</a>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

@endsection
