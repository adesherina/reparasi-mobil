@extends('user.utama.master')
@section('title', 'Reparasi Mobil')


@section('content')
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Detail Sparepart</h2>
                <p class="pageheader-text"></p>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Detail Sparepart</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Detail Sparepart</li>
                        </ol>
                    </nav>
                <div class="offset-xl-2 col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
                    <div class="row">
                        
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 pr-xl-0 pr-lg-0 pr-md-0  m-b-30">
                            <div class="product-slider">
                                <img class="d-block" src="{{url('/gambar/'.$spr->gambar)}}" alt="First slide" style="width: 285px; height:240px;">
                            
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 pl-xl-0 pl-lg-0 pl-md-0 border-left m-b-30">
                            
                            <div class="product-details">
                                <div class="border-bottom pb-3 mb-3">
                                    <input type="hidden" name="id_sparepart" id="id_sparepart-detail" value="{{$spr->id_sparepart}}" id="">
                                    <h4 class="mb-3">{{$spr->nama_sparepart}}</h4>
                                </div>
                                <div class="product-colors border-bottom">
                                    <h3 class="mb-0 text-primary">Rp. {{$spr->harga_jual}}</h3>
                                    <input type="hidden" name="harga" id="harga-detail" value="{{$spr->harga_jual}}" id="">
                                </div>
                                <div class="product-size border-bottom mb-4">
                                    <h4>Merk : {{$spr->merk}}</h4>
                                    <div class="product-qty">
                                        <h4>Quantity</h4>
                                        <div class="quantity">
                                            <input type="number" name="qty" id="qty-detail" min="1" max="9" step="1" value="1">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="_token" id="csrf-detail" value="{{Session::token()}}">
                                <div class="product-description pt-3">
                                    <input type="submit" class="btn btn-success" value="Beli Sekarang">
                                    <input type="submit" class="btn btn-primary keranjang-detail" value="Masukkan Keranjang">
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 m-b-60">
                            <div class="simple-card">
                                <ul class="nav nav-tabs" id="myTab5" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active border-left-0" id="product-tab-1" data-toggle="tab" href="#tab-1" role="tab" aria-controls="product-tab-1" aria-selected="true">Descriptions</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="product-tab-2" data-toggle="tab" href="#tab-2" role="tab" aria-controls="product-tab-2" aria-selected="false">Reviews</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent5">
                                    <div class="tab-pane fade show active" id="tab-1" role="tabpanel" aria-labelledby="product-tab-1">
                                        <p>{{$spr->deskripsi}}</p>
                                        <ul class="list-unstyled arrow">
                                            <li> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                            <li>Donec ut elit sodales, dignissim elit et, sollicitudin nulla.</li>
                                            <li>Donec at leo sed nisl vestibulum fermentum.</li>
                                        </ul>
                                    </div>
                                    <div class="tab-pane fade" id="tab-2" role="tabpanel" aria-labelledby="product-tab-2">
                                        <div class="review-block">
                                            <p class="review-text font-italic m-0">“Vestibulum cursus felis vel arcu convallis, viverra commodo felis bibendum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin non auctor est, sed lacinia velit. Orci varius natoque penatibus et magnis dis parturient montes nascetur ridiculus mus.”</p>
                                                <div class="rating-star mb-4"><i class="fa fa-fw fa-star"></i>
                                                                <i class="fa fa-fw fa-star"></i>
                                                                <i class="fa fa-fw fa-star"></i>
                                                                <i class="fa fa-fw fa-star"></i>
                                                                <i class="fa fa-fw fa-star"></i>
                                                </div>
                                                    <span class="text-dark font-weight-bold">Virgina G. Lightfoot</span><small class="text-mute"> (Company name)</small>
                                        </div>
                                        <div class="review-block border-top mt-3 pt-3">
                                            <p class="review-text font-italic m-0">“Integer pretium laoreet mi ultrices tincidunt. Suspendisse eget risus nec sapien malesuada ullamcorper eu ac sapien. Maecenas nulla orci, varius ac tincidunt non, ornare a sem. Aliquam sed massa volutpat, aliquet nibh sit amet, tincidunt ex. Donec interdum pharetra dignissim.”</p>
                                            <div class="rating-star mb-4">
                                                <i class="fa fa-fw fa-star"></i>
                                                <i class="fa fa-fw fa-star"></i>
                                                <i class="fa fa-fw fa-star"></i>
                                                <i class="fa fa-fw fa-star"></i>
                                                <i class="fa fa-fw fa-star"></i>
                                            </div>
                                                <span class="text-dark font-weight-bold">Ruby B. Matheny</span><small class="text-mute"> (Company name)</small>
                                        </div>
                                        <div class="review-block  border-top mt-3 pt-3">
                                            <p class="review-text font-italic m-0">“ Cras non rutrum neque. Sed lacinia ex elit, vel viverra nisl faucibus eu. Aenean faucibus neque vestibulum condimentum maximus. In id porttitor nisi. Quisque sit amet commodo arcu, cursus pharetra elit. Nam tincidunt lobortis augueat euismod ante sodales non. ”</p>
                                                <div class="rating-star mb-4">
                                                    <i class="fa fa-fw fa-star"></i>
                                                    <i class="fa fa-fw fa-star"></i>
                                                    <i class="fa fa-fw fa-star"></i>
                                                    <i class="fa fa-fw fa-star"></i>
                                                    <i class="fa fa-fw fa-star"></i>
                                                </div>
                                                <span class="text-dark font-weight-bold">Gloria S. Castillo</span><small class="text-mute"> (Company name)</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection