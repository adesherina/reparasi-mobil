@extends('user.utama.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Checkout</h2>
                <p class="pageheader-text"></p>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Checkout</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Checkout Anda</li>
                        </ol>
                    </nav>

                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
                        <div class="card">
                            <div class="card-header">
                                <i class="fas fa-map-marker-alt">Alamat</i> 
                            </div>
                            <div class="card-body">
                                <p>(bengkel ayip) Desa bangkaloa ilir rt 21 rw 01 kecamatan widasari kabupaten indramayu, KAB. INDRAMAYU - WIDASARI, JAWA BARAT, ID 45271</p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="tabel-data penjualan" class="table">
                                        <thead>
                                        <tr>
                                            <th>
                                            Produk dipesan
                                            </th>
                                            <th>Harga Satuan</th>
                                            <th >Jumlah</th>
                                            <th >Subtotal Produk</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <img src="" alt="Gambar Produk" class="img-fluid" style="width: 50px; height:50px;">
                                                    
                                                </td>
                                                <td>
                                                    
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="tabel-data" class="table">
                                        <thead>
                                        <tr>
                                            <th>
                                            Pengiriman
                                            </th>
                                            <th></th>
                                            <th >
                                            </th>
                                            <th ></th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody class="float-right ml-5">
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>J&T</td>
                                                <td>Rp. </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="tabel-data" class="table">
                                        <thead>
                                        <tr>
                                            <th>
                                            Pembayaran
                                            </th>
                                            <th></th>
                                            <th >
                                            </th>
                                            <th ></th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody class="float-right ml-5">
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td >Subtotal Produk</td>
                                                <td>Rp. </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>Total Pembayaran</td>
                                                <td>Rp. </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <a href="{{route('bayar')}}" class="btn btn-primary">Bayar Sekarang</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

@endsection
