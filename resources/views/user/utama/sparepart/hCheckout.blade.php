@extends('user.utama.master')
@section('title', 'Reparasi Mobil')


@section('content')

<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Checkout Sparepart</h2>
            <p class="pageheader-text"></p>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Checkout Sparepart</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Checkout Sparepart Anda</li>
                    </ol>
                </nav>

                <div class="row mt-3">
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                    
                            <div class="card card-custom bg-white border-white border-0">
                                <div class="card-custom-img " style="background-image: url('/checkout/cashier.jpg');"  ></div>
                                <div class="card-custom-avatar">
                                    <img class="img-fluid" src="{{url('checkout/circle-cropped.png')}}" alt="Avatar" />
                                </div>
                                <div class="card-body" style="overflow-y: auto">
                                    <h4 class="card-title">Nota : </h4>
                                    <p class="card-text">Tanggal Checkout : </p>
                                    <p class="card-text">Total Harga : Rp. </p>
                                </div>
                                <div class="card-footer" style="background: inherit; border-color: inherit;">
                                
                                    
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <a href="{{route('bayar')}}" class="btn btn-primary float-left" >Bayar Sekarang</a>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 col-md-6">
                                            <a href="" id="deleteCheckout" class="btn btn-danger float-right ml-2" >Batalkan Service</a>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                                
                        </div>
                       
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
