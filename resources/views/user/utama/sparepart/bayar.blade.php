@extends('user.utama.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Transaksi Sparepart</h2>
            <p class="pageheader-text"></p>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Transaksi Sparepart</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Transaksi Sparepart Anda</li>
                    </ol>
                </nav>
                 <div class="row mt-3">
                        <div class="offset-xl-2 col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="mb-0">Transaksi Sparepart</h4>
                                        </div>
                                        <div class="card-body">
                                            <form action="{{route('pesanan.simpandataTransaksi')}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                @method('patch')
                                                <div class="form-group mb-3">
                                                    <label for="firstName">Nama Lengkap</label>
                                                    <input type="text" class="form-control" name="name" value="Yuni" disabled>
                                                </div>
                                                <input type="hidden" name="kd_service" value="">
                                                <hr class="mb-4">
                                                <div class="payment">
                                                    <div class="gambar float-left mr-3">
                                                        <img src="{{url('assets/user/images/bri.png')}}" style="width: 100px; height:100px;" alt="">
                                                    </div>
                                                   <p>Silahkan bayar ke rekening : </p> 
                                                    <p> BRI : 06165-0008-0980<p>
                                                    <p> Nama : Ade Sherina<p>
                                                </div>
                                                <hr class="mb-4">
                                                <div class="form-group mb-3">
                                                    <label for="transaksi">Upload Bukti Transfer</label>
                                                    <input type="file" class="form-control" name="file" >
                                                </div>
                                                <button class="btn btn-primary btn-lg btn-block" type="submit">Lanjut Untuk Bayar</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-4">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="d-flex justify-content-between align-items-center mb-0">
                                                <span class="text-muted">Keranjang Sparepart Anda</span>
                                            </h4>
                                        </div>
                                        <div class="card-body">
                                            <ul class="list-group mb-3">
                                                <li class="list-group-item d-flex justify-content-between">
                                                    <div>
                                                        <h6 class="my-0">Jumlah Barang</h6>
                                                        <small class="text-muted">Rp. </small>
                                                    </div>
                                                </li>
                                                <li class="list-group-item d-flex justify-content-between">
                                                    <div>
                                                        <h6 class="my-0">Subtotal Sparepart</h6>
                                                        <small class="text-muted">Rp. </small>
                                                    </div>
                                                </li>
                                                <li class="list-group-item d-flex justify-content-between">
                                                    <div>
                                                        <h6 class="my-0">Total Harga</h6>
                                                        <small class="text-muted">Rp. </small>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
