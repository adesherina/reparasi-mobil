@extends('user.utama.master')
@section('title', 'Reparasi Mobil')


@section('content')

<div class="row">
    @foreach($spr as $spr)
    <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
        <div class="product-thumbnail" id="card-produk">
            <div class="product-img-head">
                <div class="product-img">
                    <img src="{{url('/gambar/'.$spr->gambar)}}" alt="" class="img-fluid" style="width: 200px; height:200px;"></div>
            </div>
            <div class="product-content">
                <div class="product-content-head">
                    <div class="nama" style="height :50px;">
                        <h5 class="product-title" >{{$spr->nama_sparepart}}</h5>
                    </div>
                    <hr class="mt-4">
                    <div class="harga mt-7">
                        <p>Rp. {{$spr->harga_jual}}</p>
                    </div>
                    <hr>
                </div>
                
                <div class="product-btn product-list">
                    <input type="submit" class="btn btn-primary keranjang2" data-produkid="{{$spr->id_sparepart}}" data-produkharga="{{$spr->harga_jual}}" data-produkqty="1" data-produkcsrf="{{Session::token()}}" value="Tambah ke Keranjang">
                    <a href="{{route('detail', $spr->id_sparepart)}}" class="btn btn-outline-light">Detail</a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection

