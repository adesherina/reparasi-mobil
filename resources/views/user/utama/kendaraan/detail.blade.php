@extends('user.utama.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Kendaraan</h2>
            <p class="pageheader-text"></p>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Kendaraan</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Kendaraan Anda</li>
                    </ol>
                </nav>
                <div class="row mt-2">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title pb-2">Kendaraan</h3>
                                <form action="{{url('garasi/'. $kendaraan->id_kendaraan)}}" id="delete{{$kendaraan->id_kendaraan}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                                    @method('delete')
                                    <button class="btn float-right btn-sm btn-outline-danger">Hapus Mobil</button>
                                    @csrf
                                </form>
                                <div class="tab mt-3">
                                    <button class="tablinks" onclick="openService(event, 'upcoming')">Informasi</button>
                                    <button class="tablinks" onclick="openService(event, 'riwayat')">Riwayat</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-2 justify-content-center">
                    <div class="col-lg-10">
                        <div class="card">
                            <div class="card-body">
                                
                                <div id="upcoming" class="tabcontent">
                                     @if ($kendaraan->jenis_mesin != null && session('statusSuccess'))
                                        <div class="alert alert-success">
                                            {{ session('statusSuccess') }}
                                        </div>
                                    @elseif($kendaraan->jenis_mesin == null && session('statusSuccess'))
                                        <div class="notFilled">
                                            Silahkan isi informasi di bawah ini. Semakin banyak informasi yang Anda berikan, semakin cepat kami bisa membuat penawaran harga untuk Anda.
                                        </div>
                                    @endif
                                    <form method="POST" action="{{url('user/utama/garasi/'.$kendaraan->id_kendaraan)}}">
                                        @csrf
                                        @method('patch')
                                        <div class="row">
                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <label for="input-select">Jenis Mesin</label>
                                                    <select class="form-control" name="jenis_mesin">
                                                        <option value="{{$kendaraan->jenis_mesin}}" selected>
                                                            @if($kendaraan->jenis_mesin == null)
                                                                Pilih Jenis Mesin
                                                            @else
                                                                {{$kendaraan->jenis_mesin}}
                                                            @endif    
                                                        </option>
                                                        <option value="Rotary">Rotary</option>
                                                        <option value="BBG">BBG</option>
                                                        <option value="Mesin Diesel ">Mesin Diesel </option>
                                                        <option value="Mesin Bensin">Mesin Bensin</option>
                                                    </select>
                                            </div>
                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <label for="input-select">Transmisi</label>
                                                    <select class="form-control" name="transmisi">
                                                        <option value="{{$kendaraan->transmisi}}" selected>
                                                            @if($kendaraan->transmisi == null)
                                                                Pilih Transmisi
                                                            @else
                                                                {{$kendaraan->transmisi}}
                                                            @endif    
                                                        </option>
                                                        <option value="Manual">Manual</option>
                                                        <option value="Automatic">Automatic</option>
                                                    </select>
                                            </div>
                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <label for="input-select">Bahan Bakar</label>
                                                    <select class="form-control" name="bahan_bakar">
                                                        <option value="{{$kendaraan->bahan_bakar}}" selected>
                                                            @if($kendaraan->bahan_bakar == null)
                                                                Pilih Bahan Bakar
                                                            @else
                                                                {{$kendaraan->bahan_bakar}}
                                                            @endif    
                                                        </option>
                                                        <option value="Pertamina Bio Solar">Pertamina Bio Solar</option>
                                                        <option value="Pertamina Bio Pertamax">Pertamina Bio Pertamax</option>
                                                        <option value="Pertamina Pertamax">Pertamina Pertamax</option>
                                                        <option value="Pertamina Dex">Pertamina Dex</option>
                                                        <option value="Pertamina Premium">Pertamina Premium</option>
                                                        <option value="Pertamina Solar">Pertamina Solar</option>
                                                    </select>
                                            </div>
                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <label for="input-select">Oli Mesin</label>
                                                    <select class="form-control" name="oli_mesin">
                                                        <option value="{{$kendaraan->oli_mesin}}" selected>
                                                            @if($kendaraan->oli_mesin == null)
                                                                Pilih Oli Mesin
                                                            @else
                                                                {{$kendaraan->oli_mesin}}
                                                            @endif    
                                                        </option>
                                                        <option value="Reguler">Reguler</option>
                                                        <option value="Sintetik">Sintetik</option>
                                                    </select>
                                            </div>
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                                <button class="btn btn-primary" type="submit">Submit form</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <?php 
                                    use App\Model\Service;
                                    use Illuminate\Support\Facades\Session;
                                    $service = Service::where('services.user_id', Session::get('id'))
                                    ->join('kendaraan', 'kendaraan.id_kendaraan', '=', 'services.kendaraan_id')
                                    ->get();
                                ?>
                                 <div id="riwayat" style="display: none;" class="tabcontent">
                                    @if($service->isEmpty())
                                        <h4 class="text-center">Halaman ini berisi riwayat Service </h4>
                                        <p class="text-center">Tidak ada Service aktif. Service sekarang!</p>
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                                            <a href="{{url('user/utama/pesananTambah')}}" class="btn btn-primary btn-lg">Service Sekarang</a>
                                        </div>
                                    @else
                                    @foreach($service as $sc)
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="card service-card" style="width: 700px;">
                                                <div class="card-body">
                                                    <div class="gambar float-left">
                                                        <img src="{{url('assets/user/images/riwayat.png')}}" style="width: 200px; height:200px;">
                                                    </div>
                                                    <div class="float-left ml-4 mt-3">
                                                        <h3 class="card-title ">Kode Pesanan : {{$sc->kd_service}}
                                                        </h3>
                                                        <p class="card-text"> Merk Kendaraan : {{$sc->merk_kendaraan}}</p>
                                                        <p class="card-text"> Jenis Service : {{$sc->jenis_service}}</p>
                                                        <p class="card-text"><small class="text-muted">Tanggal Service : {{$sc->tanggal_service}}</small></p>
                                                    </div>
                                                    <div class="float-right mt-5 mr-3">
                                                        <?php
                                                            $service2 = DB::table('services')->where('services.kd_service', $sc->kd_service)->first();                                                      
                                                        ?>
                                                        @if($service2->tindakan === "onload")
                                                         <img src="{{url('assets/user/images/people.svg')}}" style="width: 100px; height:100px;">
                                                        @elseif($service2->tindakan === "Process")
                                                        <img src="{{url('assets/user/images/loading.svg')}}" style="width: 100px; height:100px;">
                                                        @elseif($service2->tindakan === "Success")
                                                        <img src="{{url('assets/user/images/tick.svg')}}" style="width: 100px; height:100px;">
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     @endforeach
                                    @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
