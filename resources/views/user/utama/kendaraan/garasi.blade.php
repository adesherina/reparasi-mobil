@extends('user.utama.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Garasi</h2>
            <p class="pageheader-text"></p>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Garasi</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Mobil Anda</li>   
                    </ol>
                </nav>
                    @if (session('statusFailed'))
                            <div class="alert alert-danger">
                                {{ session('statusFailed') }}
                            </div>
                        @elseif(session('statusSuccess'))
                            <div class="alert alert-success">
                                {{ session('statusSuccess') }}
                            </div>
                        @endif
                <div class="row">
                    <div class="col-lg-12"><button class="float-right btn-sm btn-outline-primary" data-toggle="modal" data-target="#exampleModal">Tambah Mobil</button></div>
                         
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Tambah Mobil Baru</h5>
                                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </a>
                                    </div>
                                <div class="modal-body">
                                    <form action="{{route('garasi.simpandata')}}" method="post" id="basicform" data-parsley-validate="">
                                    @csrf
                                    @method('post')    
                                    <input type="hidden" name="user_id" value="{{Session::get('id')}}">
                                        <div class="form-group">
                                            <!-- <label for="input-select">Merk Mobil</label> -->
                                            <select class="form-control" name="merk_kendaraan">
                                                <option selected>Merk</option>
                                                <option value="Honda">Honda</option>
                                                <option value="Toyota">Toyota</option>
                                                <option value="Daihatsu">Daihatsu</option>
                                                <option value="Hino">Hino</option>
                                                <option value="Mitsubishi">Mitsubishi</option>
                                                <option value="Izuzu">Izuzu</option>
                                                <option value="Nissan">Nissan</option>
                                            </select>
                                        </div>
                                        <div class="form-group mt-3">
                                            <input type="text" class="form-control" name="model" placeholder="Model Mobil">
                                        </div>
                                        <div class="form-grup mt-3">
                                            <input type="text" class="form-control" name="tahun_keluar" placeholder="Tahun Keluar">    
                                        </div>
                                        <div class="form-grup mt-3">
                                            <input type="text" class="form-control" name="platnomor" placeholder="Plat Nomor">    
                                        </div>
                                        <div class="form-grup mt-3">
                                            <input type="text" class="form-control" name="kilometer" placeholder="Kilometer">    
                                        </div>
                                </div>
                                <div class="modal-footer">
                                            <a href="#" class="btn btn-secondary" data-dismiss="modal">Close</a>
                                            <input class="btn btn-primary" type="submit" value="Simpan">
                                        </div>
                                    </form>
                                </div>
                                        
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
                use Illuminate\Support\Facades\DB;
                use Illuminate\Support\Facades\Session;
                $kendaraan = DB::table('kendaraan')
                ->join('users', 'users.id', '=', 'kendaraan.user_id')
                ->where('id', '=', Session::get('id'))
                ->get();
                ?>
                @if($kendaraan->isEmpty())
                    <div class="row mt-2 justify-content-center">
                    <div class="col-sm-12">
                        <div class="card" style="padding:50px;"> 
                        <div class="card-body">
                            <p class="text-center"><i class="fas fa-car" style="font-size: 100px;"></i></p>
                            <h3 class="text-center">Halaman ini berisi daftar mobil yang ada di garasi Anda.</h3>
                            <p class="text-center">Anda belum menambahkan mobil ke garasi. Tambahkan mobil Anda ke garasi untuk memantau kondisi dan riwayat servisnya.</p>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                                <a href="#" data-toggle="modal" style="border-radius:50px" data-target="#exampleModal" class="btn btn-primary btn-lg">Tambah Mobil</a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                @else
                @foreach($kendaraan as $kendaraan)
                <div class="row mt-2">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                               <div class="row">
                                    <div class="col-md-4 pl-5">
                                         @if ($kendaraan->merk_kendaraan === "Honda")
                                            <img src="{{url('carLogo/honda.png')}}" width="200px" alt="">
                                        @elseif($kendaraan->merk_kendaraan === "Izuzu")
                                            <img src="{{url('carLogo/izuzu.png')}}" width="200px" alt="">
                                        @elseif($kendaraan->merk_kendaraan === "Toyota")
                                            <img src="{{url('carLogo/toyota.png')}}" width="200px" alt="">
                                        @elseif($kendaraan->merk_kendaraan === "Nissan")
                                            <img src="{{url('carLogo/nissan.png')}}" width="200px" alt="">
                                        @elseif($kendaraan->merk_kendaraan === "Daihatsu")
                                            <img src="{{url('carLogo/daihatsu.png')}}" width="200px" alt="">
                                        @elseif($kendaraan->merk_kendaraan === "Hino") 
                                            <img src="{{url('carLogo/hino.png')}}" width="200px" alt=""> 
                                        @elseif($kendaraan->merk_kendaraan === "Mitsubishi")
                                            <img src="{{url('carLogo/mitsubishi.png')}}" width="200px" alt="">      
                                        @endif
                                        <a href="{{url('user/utama/garasi/detail/'.$kendaraan->id_kendaraan)}}" style="border-radius:50px; width:180px" class="btn btn-outline-primary btn-sm ml-3 mt-3 mb-3">Lihat Informasi</a>
                                    </div>
                                    <div class="col-md-8 px-5 py-5">
                                        <p style="font-size: 16px; line-height:0.25px">{{$kendaraan->tahun_keluar}}</p>
                                        <h3>{{$kendaraan->merk_kendaraan}} {{$kendaraan->model}}</h3>
                                        <hr class="text-secondary">
                                        <div class="card mt-4" id="data-kendaraan"> 
                                            <div class="card-body">
                                                <div class="row mx-2 my-2">
                                                    <div class="col-md-5 col-sm-12 col-lg-5 text-center" >
                                                        <p>Plat Nomor</p>
                                                    </div>
                                                    <div class="col-md-7 col-sm-12 col-lg-7 text-center" >
                                                        <p id="detail-kendaraan">{{$kendaraan->platnomer}}</p>
                                                    </div>
                                                </div>
                                                <div class="row mx-2 my-2">
                                                    <div class="col-md-5 col-sm-12 col-lg-5 text-center">
                                                        <p>Kilometer</p>
                                                    </div>
                                                    <div class="col-md-7 col-sm-12 col-lg-7 text-center" >
                                                        <p id="detail-kendaraan">{{$kendaraan->kilometer}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                               </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif 
            </div>
        </div>
    </div>
</div>
@endsection
