@extends('user.utama.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Dashboard</h2>
            <p class="pageheader-text"></p>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </nav>
            </div>

            <div class="row mt-3">
                <div class="col-lg-3 col-md-3 col-sm-3 col-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="description float-left">
                            <?php 
                                use Illuminate\Support\Facades\DB;
                                use Illuminate\Support\Facades\Session;
                                    $service = DB::table('services')->where('user_id', Session::get('id'))->get();
                                    $count = $service->count();
                            ?>
                            <p style="font-size: 45px;" class="text-center">{{$count}}</p>
                            <p class="text-center" style="font-size: 12px;"> Service</p>
                            </div>
                            <div class="icon-description float-right"> 
                                <i class="fas fa-wrench mr-3 mt-3" style="font-size: 70px;"></i>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-9">
                    <div class="card">
                        <div class="card-body" style="padding: 0 !important;">
                            <div class="gambar float-left">
                                <img src="{{url('/gambar/Tune-up-mobil.jpg')}}" style="width: 145px; height:145px;">
                            </div>
                            <div class="isi float-left ml-4 mt-3">
                                <h2>Tone Up</h2>
                                <p>Harga terbaik, Meningkatkan performa mobil, Respon cepat dan mudah</p>
                                <a href="{{route('pesanan.tambahdata')}}" style="color:blue;">Service Sekarang</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                    <!-- ============================================================== -->
                    <!-- data table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="mb-0">Services</h5>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table" >
                                     
                                    <thead>
                                            <tr>
                                                <th class="text-center">Mobil</th>
                                                <th class="text-center">Plat Nomor</th>
                                                <th class="text-center">Service Terakhir</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($services as $item)  
                                            <tr>
                                                <td class="text-center">{{$item->merk_kendaraan}}</td>
                                                <td class="text-center">{{$item->platnomer}}</td>
                                                <td class="text-center">{{$item->tanggal_service}}</td>
                                            </tr>
                                            @endforeach
                                         </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end data table  -->
                    <!-- ============================================================== -->
            </div>
        </div>
    </div>
</div>
@endsection
