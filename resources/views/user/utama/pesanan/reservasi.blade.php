@extends('user.utama.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="card">
    <div class="card-body">
        <?php 
        use Illuminate\Support\Facades\DB;
             $trans = DB::table('transaksi_service')->where('kd_service', $kd_service)->first();
        ?>
        @if($trans->status == "unpaid")
        <div class="row " >
            <div class="col-lg-12 col-sm-12 col-md-12">
                 <div class="main-container">
        <div class="steps-container justify-content-center">
             <div class="step in-progress">
                 <div class="preloader"></div>
                 <div class="label loading">
                      Unpaid
                </div>
                <div class="icon in-progress">
                    <i class="far fa-money-bill-alt"></i>
                </div>
            </div>
            <div class="line prev-step-in-progress"></div>
            <div class="step">
                <div class="label">
                    Unverified
                 </div>
                    <div class="icon">
                        <i class="fas fa-spinner"></i>
                    </div>
                </div>
                <div class="line"></div>
                    <div class="step">
                        <div class="label">
                            Verified
                        </div>
                    <div class="icon">
                        <i class="fas fa-check"></i>
                    </div>
                </div>
            </div>
        </div>
            </div>
        </div>
        <div class="notSuccess text-center">
             Reservasi Berhasil Silahkan Upload bukti Pembayaran
        </div>
        <div class="row mb-4">
            <div class="col-lg-12 text-center">
                <a href="{{route('pesanan.dataTransaksi', $kd_service)}}" style="border-radius: 50px;" class="btn btn-primary">Bayar Sekarang</a>
            </div>
        </div>
        @elseif($trans->status == "unverified")
        <div class="row " >
            <div class="col-lg-12 col-sm-12 col-md-12">
                 <div class="main-container">
                <div class="steps-container justify-content-center">
                    <div class="step">
                        
                        <div class="label">
                            Unpaid
                        </div>
                        <div class="icon">
                            <i class="far fa-money-bill-alt"></i>
                        </div>
                    </div>
                    <div class="line prev-step-in-progress"></div>
                    <div class="step  in-progress">
                        <div class="preloader"></div>
                        <div class="label loading">
                            Unverified
                        </div>
                            <div class="icon in-progress">
                                <i class="fas fa-spinner"></i>
                            </div>
                        </div>
                        <div class="line"></div>
                            <div class="step">
                                <div class="label">
                                    Verified
                                </div>
                            <div class="icon">
                                <i class="fas fa-check"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="notSuccess text-center">
             Reservasi Berhasil Silahkan Tunggu Verifikasi dari Admin
        </div>
         <div class="row mb-4">
            <div class="col-lg-12 text-center">
                <a href="{{url('user/utama/checkout')}}" style="border-radius: 50px;" class="btn btn-primary">Halaman Checkout</a>
            </div>
        </div>
        @elseif($trans->status == "verified")
        <div class="row " >
            <div class="col-lg-12 col-sm-12 col-md-12">
                 <div class="main-container">
                <div class="steps-container justify-content-center">
                    <div class="step">
                        
                        <div class="label">
                            Unpaid
                        </div>
                        <div class="icon ">
                            <i class="far fa-money-bill-alt"></i>
                        </div>
                    </div>
                    <div class="line"></div>
                    <div class="step">
                        <div class="preloader"></div>
                        <div class="label">
                            Unverified
                        </div>
                            <div class="icon">
                                <i class="fas fa-spinner"></i>
                            </div>
                        </div>
                        <div class="line prev-step-in-progress"></div>
                        <div class="step  in-progress">
                        <div class="preloader"></div>
                        <div class="label loading">
                                    Verified
                                </div>
                            <div class="icon in-progress">
                                <i class="fas fa-check"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="notSuccess text-center">
            Berhasil Diverifikasi
        </div>
        <div class="row mb-4">
            <div class="col-lg-12 text-center">
                <a href="{{route('invoice',$trans->nota)}}" style="border-radius: 50px;" class="btn btn-primary">Cek Invoice</a>
            </div>
        </div>
        @endif
    </div>
</div> 
@endsection
