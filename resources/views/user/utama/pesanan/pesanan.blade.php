@extends('user.utama.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Service</h2>
            <p class="pageheader-text"></p>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Service</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Sevice Anda</li>
                    </ol>
                </nav>
                <div class="row mt-2">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title pb-2">Service</h3>
                                <div class="tab mt-3">
                                    <button class="tablinks" onclick="openService(event, 'upcoming')">Service Aktif</button>
                                    <button class="tablinks" onclick="openService(event, 'riwayat')">Riwayat Service</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-2 justify-content-center">
                    <div class="col-lg-10">
                        <div class="card" style="border-radius:60px">
                            <div class="card-body">
                                <?php 
                                    use App\Model\Service;
                                    use Illuminate\Support\Facades\Session;
                                    $service = Service::where('services.user_id', Session::get('id'))
                                    ->join('kendaraan', 'kendaraan.id_kendaraan', '=', 'services.kendaraan_id')
                                    ->get();
                                ?>
                                <div id="riwayat" style="display: none;" class="tabcontent">
                                    @if($service->isEmpty())
                                        <h4 class="text-center">Halaman ini berisi riwayat Service </h4>
                                        <p class="text-center">Tidak ada Service aktif. Service sekarang!</p>
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                                            <a href="{{url('user/utama/pesananTambah')}}" class="btn btn-primary btn-lg">Service Sekarang</a>
                                        </div>
                                    @else
                                    @foreach($service as $sc)
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="card service-card" style="width: 700px;">
                                                <div class="card-body">
                                                    <div class="gambar float-left">
                                                        <img src="{{url('assets/user/images/riwayat.png')}}" style="width: 200px; height:200px;">
                                                    </div>
                                                    <div class="float-left ml-4 mt-3">
                                                        <h3 class="card-title ">Kode Pesanan : {{$sc->kd_service}}
                                                        </h3>
                                                        <p class="card-text"> Merk Kendaraan : {{$sc->merk_kendaraan}}</p>
                                                        <p class="card-text"> Jenis Service : {{$sc->jenis_service}}</p>
                                                        <p class="card-text"><small class="text-muted">Tanggal Service : {{$sc->tanggal_service}}</small></p>
                                                    </div>
                                                    <div class="float-right mt-5 mr-3">
                                                        <?php
                                                            $service2 = DB::table('services')->where('services.kd_service', $sc->kd_service)->first();                                                      
                                                        ?>
                                                        @if($service2->tindakan === "onload")
                                                         <img src="{{url('assets/user/images/people.svg')}}" style="width: 100px; height:100px;">
                                                        @elseif($service2->tindakan === "Process")
                                                        <img src="{{url('assets/user/images/loading.svg')}}" style="width: 100px; height:100px;">
                                                        @elseif($service2->tindakan === "Success")
                                                        <img src="{{url('assets/user/images/tick.svg')}}" style="width: 100px; height:100px;">
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     @endforeach
                                        
                                </div>
                                    @endif
                                </div>
                                <div id="upcoming" class="tabcontent">
                                    @if($service->isEmpty())
                                    <h4 class="text-center">Halaman ini berisi daftar Service yang aktif</h4>
                                    <p class="text-center">Tidak ada Service aktif. Service sekarang!</p>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                                        <a href="{{url('user/utama/pesananTambah')}}" class="btn btn-primary btn-lg">Service Sekarang</a>
                                    </div>
                                    @else
                                    <div class="row" id="ads">
                                          <div class="col-md-4" >
                                            @foreach($service as $sc)
                                            <div class="card rounded">
                                                <div class="card-image">
                                                    @if($sc->tindakan == "onload")
                                                    <span class="card-notify-badge" style="background-color: #db6400;" >{{$sc->tindakan}}</span>
                                                    @elseif($sc->tindakan == "Process")
                                                    <span class="card-notify-badge" style="background-color: #5294ff;" >{{$sc->tindakan}}</span>
                                                    @elseif($sc->tindakan == "Success")
                                                    <span class="card-notify-badge" style="background-color: #45ba41;" >{{$sc->tindakan}}</span>
                                                    @endif
                                                    <span class="card-notify-year">{{$sc->tahun_keluar}}</span>
                                                    <img class="img-fluid" src="{{url('checkout/service.png')}}" alt="Alternate Text" />
                                                </div>
                                                <div class="card-image-overlay m-auto">
                                                    <span class="card-detail-badge">Total Service</span>
                                                    <span class="card-detail-badge">Rp. {{$sc->harga_sparepart + $sc->harga_service}}</span>
                                                    <span class="card-detail-badge">{{$sc->kilometer}} Km</span>
                                                </div>
                                                <div class="card-body text-center">
                                                    <div class="ad-title m-auto">
                                                        <h5></h5>
                                                    </div>
                                                    <a class="ad-btn" href="#">View</a>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
