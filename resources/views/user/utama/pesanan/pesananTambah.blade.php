@extends('user.utama.master')
@section('title', 'Reparasi Mobil')


@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Service</h2>
            <p class="pageheader-text"></p>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Service</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Sevice Anda</li>
                    </ol>
                </nav>
                <div class="row">
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                    <div class="card">
                     <h5 class="card-header">Tambah Service</h5>
                                <div class="card-body">
                                    <form action="{{route('pesanan.simpandata')}}" method="post" enctype="multipart/form-data">
                                        @method('post')
                                        @csrf
                                        <input type="hidden" name="user_id" value="{{Session::get('id')}}">
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-right">Mobil Anda</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                <select name="kendaraan_id" class="form-control" id="">
                                                    <?php
                                                        use Illuminate\Support\Facades\DB;
                                                        use Illuminate\Support\Facades\Session;
                                                        $kendaraan = DB::table('kendaraan')->where('user_id', session::get('id'))->get();
                                                    ?>
                                                    @foreach($kendaraan as $item)
                                                    <option value="{{$item->id_kendaraan}}">{{$item->merk_kendaraan}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-right">Jenis Service</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                 <select multiple="multiple" id="my-select" onchange="myFunction()" name="jenis_service[]">
                                                    <?php
                                                        $jasa = DB::table('jasa')->get();
                                                    ?>
                                                    @foreach($jasa as $jasa)
                                                    <option value="{{$jasa->nama_jasa}}" id="{{$jasa->harga_jasa}}">{{$jasa->nama_jasa}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <input type="hidden" name="harga_sparepart" id="harga_sparepart">
                                        <input type="hidden" name="harga_service" id="total" > 

                                        <input type="hidden" name="kd_service" value="<?php 
                                            function generateRandomString($length = 25) {
                                                        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                                                        $charactersLength = strlen($characters);
                                                        $randomString = '';
                                                        for ($i = 0; $i < $length; $i++) {
                                                            $randomString .= $characters[rand(0, $charactersLength - 1)];
                                                        }
                                                        return $randomString;
                                                    }
                                            echo $myRandomString = generateRandomString(10); ?>">
                                          
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-right">Keluhan</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                <textarea class="form-control" name="keluhan"></textarea>
                                            </div>
                                        </div>
                                        <input type="hidden"  name="tindakan" value="onload" >  
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-right">Tanggal Service</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                <input type="date" class="form-control" name="tanggal_service">
                                            </div>
                                        </div>
                                        <div class="form-group row text-right">
                                            <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                                <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                               
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="card">
                                <h5 class="card-header">Deskripsi Harga</h5>
                                    <div class="card-body">
                                        <div id="notif">
                                                
                                        </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection