<!doctype html>
<html lang="en">
 
<head>
    @include('user.layouts.utama.head')
</head>

<body>
    
    <div class="dashboard-main-wrapper">
        
        <!-- navbar -->
        @include('user.layouts.utama.navbar')
        
        <!-- lsidebar -->
        @include('user.layouts.utama.sidebar')

        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    @yield('content')
                </div>
            </div>
        </div>
    </div> 
    <!-- JavaScript -->
    @include('user.layouts.utama.js')
</body>
 
</html>