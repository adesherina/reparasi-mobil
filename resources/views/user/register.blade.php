@extends('user.masterUser')
@section('title', 'Reparasi Mobil')


@section('content')
<!-- <section class="contact section" id="contact" style="border: 1px solid white;"> -->
    <div class="container px-5 py-5" style="margin-top: 120px;margin-bottom:100px; background-color:#17181a;">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                    <img src="{{url('assets/img/bengkel.jpg')}}" width="600px" class="img-fluid" alt="Responsive image">  
            </div>
            <div class="ml-auto col-lg-6 col-md-6 col-12" >
                <h2 class="mb-4 pb-2 text-white" data-aos="fade-up" data-aos-delay="200">Register</h2>
                <form action="{{route('register')}}" method="post" class="contact-form webform" data-aos="fade-up" data-aos-delay="400" role="form">
                    @csrf
                    @method('post')
                    <input type="text" class="form-control" name="nama" placeholder="Nama">
                    <input type="text" class="form-control" name="no_telp" placeholder="No Telepon">
                    <input type="text" class="form-control" name="username" placeholder="Username">
                    <input type="password" class="form-control" name="password" placeholder="Password">
                    <button type="submit" class="form-control btn btn-success"  name="submit">Daftar</button>
                </form>
            </div>
        </div>
    </div>
<!-- </section> -->
@endsection