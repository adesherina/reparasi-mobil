@extends('user.masterUser')
@section('title', 'Reparasi Mobil')


@section('content')
<section id="home" >
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto col-12">
        <div class="hero-text mt-5 text-center">
          <h6 data-aos="fade-up" data-aos-delay="300">Mekanik Profesional Kami Siap Datang</h6>
          <h1 class="text-white" data-aos="fade-up" data-aos-delay="500">Perbaiki mobil anda di Bengkel Tiga Saudara</h1>
          <a href="#feature" class="btn custom-btn mt-3" data-aos="fade-up" data-aos-delay="600">Get started</a>
          <a href="#about" class="btn custom-btn bordered mt-3" data-aos="fade-up" data-aos-delay="700">learn more</a>
          </div>
      </div>
    </div>
  </div>
</section>

<section class="feature mt-5" id="feature">
  <div class="container">
    <div class="row">
      <div class="d-flex flex-column justify-content-center ml-lg-auto mr-lg-5 col-lg-5 col-md-6 col-12">
        <h2 class="mb-3" data-aos="fade-up" style="color: #66B933;"><?php 
        $user = DB::table('konten')->where('kategori', 'Home')->first();

        echo $user->judul_konten;
        ?></h2>
        <p data-aos="fade-up" class="text-white" data-aos-delay="200">
          Kebutuhan service mobil saat ini sangat beragam dan juga sudah banyak yang semakin peduli dengan sparepart atau 
          produk yang akan digunakan saat melakukan service. Jika zaman dahulu konsumen hanya datang ke bengkel dan 
          menyerahkan sepenuhnya perawatan pada montir, saat ini konsumen sudah mulai memilih sendiri produk seperti apa 
          yang akan digunakan untuk kendaraan mereka.
        </p>
        <!-- <a href="#" class="btn custom-btn bg-color mt-3" data-aos="fade-up" data-aos-delay="300" data-toggle="modal" data-target="#membershipForm">Become a member today</a> -->
      </div>
      <div class="mr-lg-auto mt-3 col-lg-4 col-md-6 col-12">
        <div class="about-working-hours">
          <div>
            <h2 class="mb-4" data-aos="fade-up" data-aos-delay="500" style="color: #66B933;">Jam Kerja</h2>
            <strong class="d-block" data-aos="fade-up" data-aos-delay="600">Minggu</strong>
            <p data-aos="fade-up" data-aos-delay="800">Tutup</p>
            <strong class="mt-3 d-block" data-aos="fade-up" data-aos-delay="700">Senin - Jumat</strong>
            <p data-aos="fade-up" data-aos-delay="800">7:00 - 21:00 </p>
            <strong class="mt-3 d-block" data-aos="fade-up" data-aos-delay="700">Sabtu</strong>
            <p data-aos="fade-up" data-aos-delay="800">7:00  - 4:00 </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- ABOUT -->
<section class="about section mt-5 mb-5" id="about">
  <div class="container">
    <div class="row">
      <div class="mt-lg-5 mb-lg-0 mb-4 col-lg-5 col-md-10 mx-auto col-12">
        <h2 class="mb-4" data-aos="fade-up" data-aos-delay="300">Hello, we are Gymso</h2>
          <p data-aos="fade-up" data-aos-delay="400">You are NOT allowed to redistribute this HTML template downloadable ZIP file on any template collection site. You are allowed to use this template for your personal or business websites.</p>
          <p data-aos="fade-up" data-aos-delay="500">If you have any question regarding <a rel="nofollow" href="https://www.tooplate.com/view/2119-gymso-fitness" target="_parent">Gymso Fitness HTML template</a>, you can <a rel="nofollow" href="https://www.tooplate.com/contact" target="_parent">contact Tooplate</a> immediately. Thank you.</p>
      </div>
      <div class="ml-lg-auto col-lg-3 col-md-6 col-12" data-aos="fade-up" data-aos-delay="700">
        <div class="team-thumb">
          <img src="images/team/team-image.jpg" class="img-fluid" alt="Trainer">
          <div class="team-info d-flex flex-column">
            <h3>Mary Yan</h3>
            <span>Yoga Instructor</span>
            <ul class="social-icon mt-3">
              <li><a href="#" class="fa fa-twitter"></a></li>
              <li><a href="#" class="fa fa-instagram"></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="mr-lg-auto mt-5 mt-lg-0 mt-md-0 col-lg-3 col-md-6 col-12" data-aos="fade-up" data-aos-delay="800">
        <div class="team-thumb">
          <img src="images/team/team-image01.jpg" class="img-fluid" alt="Trainer">
          <div class="team-info d-flex flex-column">
            <h3>Catherina</h3>
            <span>Body trainer</span>
            <ul class="social-icon mt-3">
              <li><a href="#" class="fa fa-instagram"></a></li>
              <li><a href="#" class="fa fa-facebook"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection

