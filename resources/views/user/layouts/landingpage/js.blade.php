<script src="{{url('assets/user/js/jquery.min.js')}}"></script>
<script src="{{url('assets/user/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/user/js/aos.js')}}"></script>
<script src="{{url('assets/user/js/smoothscroll.js')}}"></script>
<script src="{{url('assets/user/js/custom.js')}}"></script>