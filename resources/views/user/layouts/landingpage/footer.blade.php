<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="ml-auto col-lg-4 col-md-5">
              <p class="copyright-text">Copyright &copy; 2020 Reparasi Mobil.com</p>
            </div>

            <div class="d-flex justify-content-center mx-auto col-lg-5 col-md-7 col-12">
                <p class="mr-4">
                    <i class="fa fa-envelope-o mr-1"></i>
                    <a href="#">Reparasi Mobil@company.com</a>
                </p>

                <p><i class="fa fa-phone mr-1"></i> 0895379282402</p>
            </div>            
        </div>
    </div>
</footer>