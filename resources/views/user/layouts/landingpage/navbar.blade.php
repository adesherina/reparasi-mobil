<nav class="navbar navbar-expand-lg fixed-top">
        <div class="container">

            <a class="navbar-brand" href="#">Reparasi Mobil</a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-lg-auto">
                    <li class="nav-item">
                        <a href="{{url('/')}}" class="nav-link smoothScroll">Home</a>
                    </li>

                    <li class="nav-item">
                        <a href="{{url('user/layanan')}}" class="nav-link smoothScroll">Layanan</a>
                    </li>

                    <li class="nav-item">
                        <a href="{{url('user/carakerja')}}" class="nav-link smoothScroll">Cara Kerja</a>
                    </li>

                    <li class="nav-item">
                        <a href="{{url('user/kontak')}}" class="nav-link smoothScroll">Kontak</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('login2')}}" class="btn btn-outline-success ml-3"> Login </a>
                    </li>
                </ul>
            </div>

        </div>
    </nav>