<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{url('assets/user/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link href="{{url('assets/user/vendor/fonts/circular-std/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{url('assets/user/libs/css/style.css')}}">
    <link rel="stylesheet" href="{{url('assets/user/vendor/fonts/fontawesome/css/fontawesome-all.css')}}">
    <link rel="stylesheet" href="{{url('assets/user/vendor/charts/chartist-bundle/chartist.css')}}">
    <link rel="stylesheet" href="{{url('assets/user/vendor/charts/morris-bundle/morris.css')}}">
    <link rel="stylesheet" href="{{url('assets/user/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/user/vendor/charts/c3charts/c3.css')}}">
    <link rel="stylesheet" href="{{url('assets/user/vendor/fonts/flag-icon-css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/user/customstyle.css')}}">
    <link rel="stylesheet" href="{{url('assets/user/custom.scss')}}">
    
    <title>Reperasi Mobil</title>
    <link rel="stylesheet" href="{{url('assets/user/vendor/multi-select/css/multi-select.css')}}">
    
    
    <link rel="stylesheet" type="text/css" href="{{url('assets/user/vendor/datatables/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/user/vendor/datatables/css/buttons.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/user/vendor/datatables/css/select.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/user/vendor/datatables/css/fixedHeader.bootstrap4.css')}}">


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> 