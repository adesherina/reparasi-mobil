<div class="nav-left-sidebar sidebar-dark">
    <div class="menu-list">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav flex-column">
                        
                        <li class="nav-item ">
                            <a class="nav-link" href="{{url('user/utama/dashboard')}}" aria-controls="submenu-1"><i class="fas fa-home"></i>Dashboard <span class="badge badge-success">6</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('user/utama/garasi')}}" aria-controls="submenu-2"><i class="fas fa-tachometer-alt"></i>Garasi</a>
                        </li>
                        <li class="nav-divider">
                            Service
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('user/utama/pesanan')}}" aria-controls="submenu-3"><i class="fas fa-car"></i>Service</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('user/utama/checkout')}}"><i class="fas fa-cart-plus"></i>Service Checkout</a>
                        </li>
                        <li class="nav-divider">
                            Sparepart
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('sparepart')}}" aria-controls="submenu-4"><i class="fas fa-venus-mars"></i>Sparepart</a>
                        </li>
                        <li class="nav-item">
                             <a class="nav-link" href="{{route('keranjang')}}" id="navbarDropdown" role="button"  > <i class=" fas fa-shopping-cart"></i> Keranjang</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('user/utama/sparepart/hCheckout')}}"><i class="fas fa-cart-plus"></i>Sparepart Checkout</a>
                        </li>
                        <li class="nav-divider">
                            Data Akun
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('user/update/'.Session::get('id'))}}"><i class="fas fa-user mr-2"></i>Account</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('logout2')}}"><i class="fas fa-power-off mr-2"></i>Logout</a>
                        </li>
                    </ul>
                </div>  
        </nav>
    </div>
</div>