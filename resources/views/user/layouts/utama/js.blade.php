
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="{{url('assets/user/vendor/bootstrap/js/bootstrap.bundle.js')}}"></script>
    <!-- slimscroll js -->
    <script src="{{url('assets/user/vendor/slimscroll/jquery.slimscroll.js')}}"></script>
    <!-- main js -->
    <script src="{{url('assets/user/libs/js/main-js.js')}}"></script>
    <!-- chart chartist js -->
    <script src="{{url('assets/user/vendor/charts/chartist-bundle/chartist.min.js')}}"></script>
    <!-- sparkline js -->
    <script src="{{url('assets/user/vendor/charts/sparkline/jquery.sparkline.js')}}"></script>
    <!-- morris js -->
    <script src="{{url('assets/user/customjs.js')}}"></script>
    
    <script src="{{url('assets/user/customAjax.js')}}"></script>
    <script src="{{url('assets/user/vendor/charts/morris-bundle/raphael.min.js')}}"></script>
    <script src="{{url('assets/user/vendor/charts/morris-bundle/morris.js')}}"></script>
    <!-- chart c3 js -->
    <script src="{{url('assets/user/vendor/charts/c3charts/c3.min.js')}}"></script>
    <script src="{{url('assets/user/vendor/charts/c3charts/d3-5.4.0.min.js')}}"></script>
    <script src="{{url('assets/user/vendor/charts/c3charts/C3chartjs.js')}}"></script>
    <script src="{{url('assets/user/libs/js/dashboard-ecommerce.js')}}"></script>

    <script src=".{{url('assets/user/vendor/multi-select/js/jquery.multi-select.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="{{url('assets/user/vendor/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="{{url('assets/user/vendor/datatables/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{url('assets/user/vendor/datatables/js/data-table.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    <script src="{{url('assets/user/vendor/multi-select/js/jquery.multi-select.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>
    <script>
    $('#my-select, #pre-selected-options').multiSelect()
    </script>
    <script>
    $('#callbacks').multiSelect({
        afterSelect: function(values) {
            alert("Select value: " + values);
        },
        afterDeselect: function(values) {
            alert("Deselect value: " + values);
        }
    });
    </script>
    <script>
    $('#keep-order').multiSelect({ keepOrder: true });
    </script>
    <script>
    $('#public-methods').multiSelect();
    $('#select-all').click(function() {
        $('#public-methods').multiSelect('select_all');
        return false;
    });
    $('#deselect-all').click(function() {
        $('#public-methods').multiSelect('deselect_all');
        return false;
    });
    $('#select-100').click(function() {
        $('#public-methods').multiSelect('select', ['elem_0', 'elem_1',... 'elem_99']);
        return false;
    });
    $('#deselect-100').click(function() {
        $('#public-methods').multiSelect('deselect', ['elem_0', 'elem_1',... 'elem_99']);
        return false;
    });
    $('#refresh').on('click', function() {
        $('#public-methods').multiSelect('refresh');
        return false;
    });
    $('#add-option').on('click', function() {
        $('#public-methods').multiSelect('addOption', { value: 42, text: 'test 42', index: 0 });
        return false;
    });
    </script>
    <script>
    $('#optgroup').multiSelect({ selectableOptgroup: true });
    </script>
    <script>
    $('#disabled-attribute').multiSelect();
    </script>
    <script>
    $('#custom-headers').multiSelect({
        selectableHeader: "<div class='custom-header'>Selectable items</div>",
        selectionHeader: "<div class='custom-header'>Selection items</div>",
        selectableFooter: "<div class='custom-header'>Selectable footer</div>",
        selectionFooter: "<div class='custom-header'>Selection footer</div>"
    });
    </script>
     
<!-- QTY -->
    <script>
        

    $(document).ready(function () {

        $("#checkAll").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
            calcTotal()
        });


        $('#deleteAll').on('click', function(e) {


            var allVals = [];  

            $(".checkedItem:checked").each(function() {  

                allVals.push($(this).attr('data-id'));

            });  


            if(allVals.length <=0)  

            {  

                alert("Please select row.");  

            }  else {  


                var check = confirm("Are you sure you want to delete this row?");  

                if(check == true){  


                    var join_selected_values = allVals.join(","); 


                    $.ajax({

                        url: $(this).data('url'),

                        type: 'DELETE',

                        headers: {'X-CSRF-TOKEN': $(this).data("csrf")},

                        data: 'id='+join_selected_values,

                        success: function (data) {

                            if (data['success']) {

                                $(".checkedItem:checked").each(function() {  

                                    $(this).parents("tr").remove();

                                });

                                alert(data['success']);

                            } else if (data['error']) {

                                alert(data['error']);

                            } else {

                                alert('Whoops Something went wrong!!');

                            }

                        },

                        error: function (data) {

                            alert(data.responseText);

                        }

                    });


                  $.each(allVals, function( index, value ) {

                      $('table tr').filter("[data-row-id='" + value + "']").remove();

                  });

                }  

            }  

        });




        


    });

        jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
        
        jQuery('.quantity').each(function() {
            var spinner = jQuery(this),
                input = spinner.find('input[type="number"]'),
                btnUp = spinner.find('.quantity-up'),
                btnDown = spinner.find('.quantity-down'),
                min = input.attr('min'),
                max = input.attr('max');
            

            btnUp.click(function() {
                var oldValue = parseFloat(input.val());
                
                if (oldValue >= max) {
                    var newVal = oldValue;
                } else {
                    var newVal = oldValue + 1;
                }
                spinner.find("input").val(newVal);
                
                spinner.find("input").trigger("change");
                
            });

            btnDown.click(function() {
                var oldValue = parseFloat(input.val());
                if (oldValue <= min) {
                    var newVal = oldValue;
                } else {
                    var newVal = oldValue - 1;
                }
                spinner.find("input").val(newVal);
                spinner.find("input").trigger("change");
            });

        });
</script>

<!-- hitung sub total dan total harga-->
 <script>
      $('.penjualan').each(function() {
            $(this).parent().find('.qty2').keyup(function(){
               var value1 = parseFloat($(this).parent().find('.qty2').val()) || 0;
               var value2 = parseFloat($(this).parent().prev().children().text()) || 0;
               console.log(value1,value2);
            
            $(this).parent().next().find('.subtotal').text(value1 * value2);
            if($(this).parent().prev().prev().prev().find('.checkedItem').is(':checked') === true)
            {
             calcTotal();   
            }
            
            });
         });
          $('#total').text('Rp.0');
        $(".checkedItem").click(function(event) {
            var total = 0;
            $(".checkedItem:checked").each(function() {
                total += parseInt($(this).parent().next().next().next().next().find('.subtotal').text());
            });
            
            if (total == 0) {
                console.log(total);
                $('#total').text('Rp.0');
            } else {
                console.log(total);                
                $('#total').text(`Rp. `+total);
            }
        });

            // $('#pricelist :checkbox').click(function() {
            // calcAndShowTotal();
            // });

            // calcAndShowTotal();

         
         function calcTotal(){
            var sum = 0;
            $('.subtotal').each(function(){
                if($(this).parent().prev().prev().prev().prev().find('.checkedItem').is(':checked') === true){
                    sum += +$(this).text();
                }
                
            });
            $('#total').text(`Rp.`+sum);
         }
        
        // console.log(sum);
    
    $('.checkedItem').on('click', function(event) {
        var checkboxValues = [];
        if($(this).is(':checked') == true) {
            // var object = 
            // var params = JSON.stringify(object);
            // console.log(object);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                dataType : 'json',
                type: 'POST',
                data: {
                    id: $(this).data("id"),
                    qty: $(this).data("qty"),
                    harga: $(this).data("harga")
                },
                url: 'http://127.0.0.1:8000/user/utama/sparepart/jsonadd',
                success: function(data){
                    console.log(data);
                },
                error: function(){
                    
                }
            });
        }
        // console.log(checkboxValues);
        // console.log($(this));
    });
        
</script>
