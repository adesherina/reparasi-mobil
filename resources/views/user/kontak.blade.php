@extends('user.masterUser')
@section('title', 'Reparasi Mobil')


@section('content')
<!-- <section class="contact section" id="contact" style="border: 1px solid white;"> -->
    <div class="container bg-dark" style="margin-top: 100px;margin-bottom:100px; ">
        <div class="row" >
            <div class="ml-auto col-lg-6 col-md-6 col-12 " >
                <h2 class="mb-4 pb-2 text-white" data-aos="fade-up" data-aos-delay="200">Kritik dan Saran</h2>
                <form action="{{route('user.addKS')}}" method="post" class="contact-form webform" data-aos="fade-up" data-aos-delay="400" role="form">
                    @csrf
                    @method('post')
                    <input type="text" class="form-control" name="nama" placeholder="Nama">
                    <textarea class="form-control" rows="5" name="isi" placeholder="Kritik dan Saran"></textarea>
                    <button type="submit" class="form-control btn btn-success"  name="submit">Send Message</button>
                </form>
            </div>
            <div class="mx-auto mt-4 mt-lg-0 mt-md-0 col-lg-6 col-md-6 col-12">
                 <h2 class="mb-4 text-white" data-aos="fade-up" data-aos-delay="600">Where you can <span>find us</span></h2>
                 <p data-aos="fade-up" data-aos-delay="800" class="text-white"><i class="fa fa-map-marker mr-1"></i>Bangkaloa Ilir - Widasari - Indramayu</p>
                <div class="google-map" data-aos="fade-up" data-aos-delay="900">
                     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15856.936941394562!2d108.29691467495003!3d-6.491997718075826!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6ec686b86e348d%3A0xfe852390a4354cd!2sBangkaloa%20Ilir%2C%20Kec.%20Widasari%2C%20Kabupaten%20Indramayu%2C%20Jawa%20Barat!5e0!3m2!1sid!2sid!4v1604456375876!5m2!1sid!2sid" width="1920" height="250" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
    </div>
<!-- </section> -->
@endsection