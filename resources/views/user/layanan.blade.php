@extends('user.masterUser')
@section('title', 'Reparasi Mobil')


@section('content')
<section class="class section" id="class">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12 text-center mb-5">
                 <h6 data-aos="fade-up" class="text-white">Dapatkan Layanan Terbaik dengan Harga Terjangkau</h6>
            </div>
            <div class="col-lg-4 col-md-6 col-12" data-aos="fade-up" data-aos-delay="400">
                <div class="class-thumb">
                    <!-- <img src="images/class/yoga-class.jpg" class="img-fluid" alt="Class"> -->
                    <div class="class-info">
                        <h3 class="mb-1">Ganti Oli</h3>
                        <p class="mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                        <a href="">Selengkapnya<i class="fas fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="mt-5 mt-lg-0 mt-md-0 col-lg-4 col-md-6 col-12" data-aos="fade-up" data-aos-delay="500">
                <div class="class-thumb">
                    <!-- <img src="images/class/crossfit-class.jpg" class="img-fluid" alt="Class"> -->
                    <div class="class-info">
                         <h3 class="mb-1">Ganti Aki Mobil</h3>
                         <p class="mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                         <a href="">Selengkapnya<i class="fas fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="mt-5 mt-lg-0 col-lg-4 col-md-6 col-12" data-aos="fade-up" data-aos-delay="600">
                <div class="class-thumb">
                     <div class="class-info">
                         <h3 class="mb-1">Ganti Minyak Kopling</h3>
                         <p class="mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                         <a href="">Selengkapnya<i class="fas fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection