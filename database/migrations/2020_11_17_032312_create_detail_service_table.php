<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_service', function (Blueprint $table) {
            $table->string('kd_service', 12);
            $table->integer('id_sparepart')->unsigned();
            $table->integer('jumlah');
            $table->integer('subtotal');

            $table->foreign('id_sparepart')->references('id_sparepart')->on('spareparts');
             $table->foreign('kd_service')->references('kd_service')->on('services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_service');
    }
}
