<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKendaraan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kendaraan', function (Blueprint $table) {
            $table->increments('id_kendaraan');
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('merk_kendaraan', 100);
            $table->string('jenis_mesin', 100)->nullable();
            $table->string('platnomer', 10);
            $table->integer('tahun_keluar');
            $table->integer('kilometer');
            $table->string('model', 100);
            $table->string('transmisi', 100)->nullable();
            $table->string('bahan_bakar', 100)->nullable();
            $table->string('oli_mesin', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kendaraan');
    }
}
