<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->string('kd_service', 12)->primary();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('kendaraan_id')->unsigned();
            $table->bigInteger('jasa_id')->unsigned();
            $table->string('jenis_service', 100);
            $table->string('keluhan', 100);
            $table->string('tindakan', 100);
            $table->date('tanggal_service');

            $table->foreign('kendaraan_id')->references('id_kendaraan')->on('kendaraan')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('jasa_id')->references('id_jasa')->on('jasa')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service');
    }
}
