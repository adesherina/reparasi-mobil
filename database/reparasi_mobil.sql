-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2021 at 05:12 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `reparasi_mobil`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `nama`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, 'ades', 'ades', '$2y$10$UXPVsQcp8lc0b7WtgZww3O8Bk6L68vMOay9IO4Ml7V.TN89vMsz1a', NULL, NULL),
(4, 'wantrisnadi', 'wacik', '$2y$10$xe3tBY5IM1L6jHq8st5CLOh/CHi6mNUJnashxYasfhD4bJeSgSKZi', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `detail_penjualan`
--

CREATE TABLE `detail_penjualan` (
  `kd_nota` varchar(20) NOT NULL,
  `id_keranjang` int(10) NOT NULL,
  `id_sparepart` int(10) NOT NULL,
  `qty` int(10) NOT NULL,
  `harga` int(20) NOT NULL,
  `subtotal` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jasa`
--

CREATE TABLE `jasa` (
  `kd_jasa` int(11) NOT NULL,
  `nama_jasa` varchar(10) NOT NULL,
  `harga_jasa` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jasa`
--

INSERT INTO `jasa` (`kd_jasa`, `nama_jasa`, `harga_jasa`) VALUES
(1, 'Tone Up', 200000);

-- --------------------------------------------------------

--
-- Table structure for table `kendaraan`
--

CREATE TABLE `kendaraan` (
  `id_kendaraan` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `merk_kendaraan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_mesin` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `platnomer` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun_keluar` int(11) NOT NULL,
  `kilometer` int(11) NOT NULL,
  `model` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transmisi` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bahan_bakar` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oli_mesin` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kendaraan`
--

INSERT INTO `kendaraan` (`id_kendaraan`, `user_id`, `merk_kendaraan`, `jenis_mesin`, `platnomer`, `tahun_keluar`, `kilometer`, `model`, `transmisi`, `bahan_bakar`, `oli_mesin`) VALUES
(1, 6, 'Honda', 'Mesin Bensin', 'E5480RZ', 2014, 2000, 'Jazz', 'Manual', 'Pertamina Pertamax', 'Reguler'),
(2, 8, 'Toyota', NULL, 'E5432PZ', 2010, 10000, 'Diesel', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `keranjang`
--

CREATE TABLE `keranjang` (
  `id_keranjang` int(10) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `id_sparepart` int(10) UNSIGNED NOT NULL,
  `harga` int(20) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `subtotal` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `keranjang`
--

INSERT INTO `keranjang` (`id_keranjang`, `user_id`, `id_sparepart`, `harga`, `jumlah`, `subtotal`) VALUES
(108, 8, 8, 320000, 1, 320000);

-- --------------------------------------------------------

--
-- Table structure for table `konten`
--

CREATE TABLE `konten` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `judul_konten` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi_konten` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `konten`
--

INSERT INTO `konten` (`id`, `judul_konten`, `isi_konten`, `kategori`) VALUES
(1, 'Mobil bermasalah?', 'Jadwalkan servis mobil Anda online dan mekanik kami akan datang dimanapun Anda berada. Semua prosesnya sangat mudah. Dengan menggunakan website kami, Anda bisa mengatur jadwal servis, mendapatkan penawaran harga, dan melihat riwayat servis mobil Anda. Selain itu harga servis kami juga lebih rendah dari harga bengkel karena kami tidak mempunyai bengkel fisik sehingga biaya operasional kami lebih rendah.', 'Home'),
(2, 'Booking servis Anda dengan mudah', '1. Masukan informasi mobil Anda bedasarkan merek, tahun, dan tipe\r\n2. Pilih jenis servis dan sparepart yang Anda butuhkan\r\n3. Berikan informasi Anda (nama, lokasi, nomor telpon, dll)', 'Layanan');

-- --------------------------------------------------------

--
-- Table structure for table `kritiksaran`
--

CREATE TABLE `kritiksaran` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kritiksaran`
--

INSERT INTO `kritiksaran` (`id`, `nama`, `isi`, `created_at`, `updated_at`) VALUES
(1, 'ades', 'bengkel ini sangat bagus', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(41, '2014_10_12_000000_create_users_table', 1),
(42, '2020_11_03_015828_create_konten', 1),
(43, '2020_11_03_223817_create_kritiksaran_table', 1),
(44, '2020_11_03_232747_create_admin_table', 1),
(52, '2020_11_09_062928_create_sparepart_table', 4),
(72, '2020_11_09_084054_create_kendaraan', 5);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `kd_nota` varchar(20) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `tanggal_nota` date NOT NULL,
  `total_bayar` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `kd_service` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `kendaraan_id` int(10) UNSIGNED NOT NULL,
  `jenis_service` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_service` int(10) NOT NULL,
  `harga_sparepart` int(10) NOT NULL,
  `keluhan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tindakan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_service` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`kd_service`, `user_id`, `kendaraan_id`, `jenis_service`, `harga_service`, `harga_sparepart`, `keluhan`, `tindakan`, `tanggal_service`) VALUES
('0Q3FVNRY29', 6, 1, 'Tone Up', 80000, 0, 'service', 'onload', '2020-11-30'),
('31KRANVGEB', 6, 1, 'Tone Up', 80000, 0, 'hh', 'onload', '2020-11-25'),
('4TY13NAOCI', 8, 2, 'Tone Up', 200000, 0, 'ahahja', 'onload', '2021-03-16'),
('6K405QLCX8', 6, 1, 'Tone Up, Ganti Aki, Ganti Oli', 205000, 90000, 'service', 'onload', '2020-12-23'),
('A3GN88JFEV', 6, 1, 'Tone Up', 80000, 0, 'service', 'Process', '2020-11-25'),
('TVMUH6N5LJ', 6, 1, 'Ganti Oli', 45000, 45000, 'ganti', 'Success', '2020-11-25'),
('YDMYAOADGK', 6, 1, 'Ganti Aki', 80000, 45000, 'aki bocor', 'onload', '2020-12-02');

-- --------------------------------------------------------

--
-- Table structure for table `spareparts`
--

CREATE TABLE `spareparts` (
  `id_sparepart` int(10) UNSIGNED NOT NULL,
  `merk` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_sparepart` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `stok` int(20) NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `spareparts`
--

INSERT INTO `spareparts` (`id_sparepart`, `merk`, `nama_sparepart`, `harga_beli`, `harga_jual`, `stok`, `deskripsi`, `gambar`) VALUES
(8, 'Toyota', 'Fan Kipas Radiator Dan Ac', 299000, 320000, 5, 'Motor fan atau motor kipas radiator dan ac untuk kendaraan Toyota New Vios dan Yaris tahun 2006 sampai dengan tahun 2013. Made in RRC didistribusikan oleh PT Densocentra Indonesia.', 'Fan Kipas Radiator Dan Ac.jpg'),
(9, 'SHELL Helix', 'Oli Mobil SHELL Helix HX5 15W 40 1 liter', 54000, 60000, 2, 'Isi : 1000 ml / 1 liter\r\n Active Cleansing Teknologi, membersihkan dan menjaga mesin tetap bersih, terasa seperti baru\r\nTeknologi sintetik, pelumas tahan lebih lama dibandingkan teknologi mineral\r\nCocok untuk mesin modern, menghindari gejala knocking pada mesin turbocharge', 'olihelix.jpeg'),
(10, 'Carrera', 'Carrera Radiator Coolant 5000ml', 66500, 70000, 3, 'CARRERA Radiator Coolant dengan formula khusus , efektif untuk menstabilkan suhu mesin dari panas berlebih dan meningkatkan titik didih. Mencegah dan melindungi komponen sistem pendingin dari pembentukan karat,korosi dan pengendapan kerak. Membentuk lapisan pelindung pada bagian sistem pendingin yg terbuat dari metal /aluminium , dg proses demineralisasi. Manfaat ; - praktis penggunaan, - harga ekonomis, - menurunkan suhu didih sehingga mobil lebih dingin dan tdk boros, - melindungi metal dan aluminium, - tdk mengandung kadar mineral tinggi yg dpt membuat karat.', 'carrera.jpg'),
(11, 'Nissan', 'Ignition Coil Nissan Teana VQ23 VQ25 V6 2003-2013 22448-8J115 10004617', 236592, 260000, 1, 'Deskripsi Ignition Coil Nissan Teana VQ23 VQ25 V6 2003-2013 22448-8J115 10004617', 'ignition coilnisan.jpeg'),
(12, 'PRESTONE', 'PRESTONE [Bundle] Ready To Use Green 1L', 75200, 82000, 6, 'PRESTONE READY TO USE COOLANT (33%) GREEN 1 L (isi 2pc)\r\n\r\nPrestone Ready to Use Radiator Coolant mampu memberi pendinginan mesin yang sempurna. Prestone Ready to Use Radiator Coolant pendingin dengan komposisi yang tepat dari coolant dan air, tidak perlu air tambahan pada saat mengisikannya.\r\n\r\nIsi berwarna Hijau. Tidak ada perbedaan kualitas dan spesifikasi untuk varian warna Pink & Hijau untuk coolant Prestone. Coolant Prestone dapat di pakai untuk SEMUA Merek dan Model kendaraan, maupun di campur dengan semua warna dan merek coolant.\r\nPrediluted 33%. Siap Pakai.', 'prestone.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_service`
--

CREATE TABLE `transaksi_service` (
  `nota` varchar(12) NOT NULL,
  `kd_service` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_user` varchar(100) NOT NULL,
  `total_harga` int(10) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi_service`
--

INSERT INTO `transaksi_service` (`nota`, `kd_service`, `nama_user`, `total_harga`, `tgl_transaksi`, `file`, `status`) VALUES
('5VC4PQCUID', '0Q3FVNRY29', 'yuni', 80000, '2020-11-30', 'WhatsApp Image 2020-11-19 at 10.04.32.jpeg', 'unverified'),
('64P89DILP6', 'YDMYAOADGK', 'yuni', 125000, '2020-12-02', NULL, 'unpaid'),
('D5SG26VYAX', '4TY13NAOCI', 'ina', 200000, '2021-03-08', 'user.png', 'unverified'),
('IU52E2EN4G', 'A3GN88JFEV', 'yuni', 80000, '2020-11-25', '3281755.jpg', 'verified'),
('QH56FZREIK', '6K405QLCX8', 'yuni', 295000, '2020-12-07', NULL, 'unpaid'),
('TU9KN3MG1H', '31KRANVGEB', 'yuni', 80000, '2020-11-25', 'Picture1.png', 'verified');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode_pos` int(10) DEFAULT NULL,
  `jenis_kelamin` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `no_telp`, `alamat`, `kode_pos`, `jenis_kelamin`, `foto`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(6, 'yuni', '0895379282402', 'Jl. Jendral Sudirman No.2422', 45271, 'Perempuan', '37298__2_-removebg-preview.png', 'yuni', '$2y$10$JI7j7W8JPGyvg6wnkoPUyOoIBbEHLyVjaW6W52kvL7dEisoGZsV6.', NULL, NULL, NULL),
(7, 'ade sherina', '081295491852', NULL, NULL, NULL, NULL, 'ade', '$2y$10$0WE82iNGpE4bk1hA1ZyqHuVbsXTnqcpUvJsdTd0l8Wlgf4KT1PLgO', NULL, NULL, NULL),
(8, 'ina', '082115806156', 'Widassari Indramayu', 45271, 'Perempuan', 'img1.jpg', 'ina', '$2y$10$kcVlqZnVVoP7D3v10yHohOtgjEgaExBuDEGXSycdMET8mp5nghLXW', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_username_unique` (`username`);

--
-- Indexes for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD KEY `nota` (`kd_nota`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jasa`
--
ALTER TABLE `jasa`
  ADD PRIMARY KEY (`kd_jasa`);

--
-- Indexes for table `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD PRIMARY KEY (`id_kendaraan`),
  ADD KEY `kendaraan_user_id_foreign` (`user_id`);

--
-- Indexes for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD PRIMARY KEY (`id_keranjang`),
  ADD KEY `id_sparepart` (`id_sparepart`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `konten`
--
ALTER TABLE `konten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kritiksaran`
--
ALTER TABLE `kritiksaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`kd_nota`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`kd_service`),
  ADD KEY `services_user_id_foreign` (`user_id`),
  ADD KEY `services_kendaraan_id_foreign` (`kendaraan_id`);

--
-- Indexes for table `spareparts`
--
ALTER TABLE `spareparts`
  ADD PRIMARY KEY (`id_sparepart`);

--
-- Indexes for table `transaksi_service`
--
ALTER TABLE `transaksi_service`
  ADD PRIMARY KEY (`nota`),
  ADD KEY `kd_service` (`kd_service`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_no_telp_unique` (`no_telp`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jasa`
--
ALTER TABLE `jasa`
  MODIFY `kd_jasa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kendaraan`
--
ALTER TABLE `kendaraan`
  MODIFY `id_kendaraan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `keranjang`
--
ALTER TABLE `keranjang`
  MODIFY `id_keranjang` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `konten`
--
ALTER TABLE `konten`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kritiksaran`
--
ALTER TABLE `kritiksaran`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `spareparts`
--
ALTER TABLE `spareparts`
  MODIFY `id_sparepart` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD CONSTRAINT `detail_penjualan_ibfk_1` FOREIGN KEY (`kd_nota`) REFERENCES `penjualan` (`kd_nota`);

--
-- Constraints for table `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD CONSTRAINT `kendaraan_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD CONSTRAINT `keranjang_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `keranjang_ibfk_2` FOREIGN KEY (`id_sparepart`) REFERENCES `spareparts` (`id_sparepart`);

--
-- Constraints for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD CONSTRAINT `penjualan_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_kendaraan_id_foreign` FOREIGN KEY (`kendaraan_id`) REFERENCES `kendaraan` (`id_kendaraan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `services_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi_service`
--
ALTER TABLE `transaksi_service`
  ADD CONSTRAINT `transaksi_service_ibfk_1` FOREIGN KEY (`kd_service`) REFERENCES `services` (`kd_service`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
